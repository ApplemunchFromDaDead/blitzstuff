# Blitz's Stuff

This is a repository of all sorts of miscellaneous content I've made, including:
- Drawings
- Scripts (shell scripts)
- Texts
- Miscellaneous projects without their own repository

I promise that all creations and creative works outside of the **NONFREE** folder are free culture works, most of them CC0, some are other licenses yet still free (GPL, BSD3, etc.)

The **DRAWINGS** includes drawing and their .xcf source files (if present) See the LICENSES in the folder for detailed licensing information. The **NSFW** subfolder does not contain such content directly, should it not be edited at all. Links to the actual NSFW content elsewhere could be provided.

The **NONFREE** folder contains **non-libre data** that I decided to keep here solely for archival purposes. **DO NOT LOOK IN THIS FOLDER IF YOU ARE LOOKING FOR REUSABLE ASSETS.**

## Drawings Info

*See WRITINGS/BlitzverseLore.txt for contextual information.*

Many of these, if not all of them, are self-made drawings I made in GIMP. Any images that contained non-free content have been edited from the original to liberate them; the originals are still available in the NONFREE folder. I'd have XCFs of many of the Redmond and Bluu drawings, but I lost them, among many things, to a hard-drive format somewhere in early 2023.

## Music Info

Many of the MIDIs were exported from LMMS and given back instruments and pitch bends through the Signal MIDI editor.

The BIFF_* MIDIs were made for Biff Fraggar's Libre Adventure, a FOSS Doom IWAD replacement inspired by Freedoom. While BFLA is GPL'd, these specific tracks are CC0 thanks to me being the creator.

The JWLA_* and FAZER_* MIDIs were made for my projects John Williams' Adventure (2024) and Fazer respectively. semantics.mid is from Court of Loons.

The MUGEN_* MIDIs were made for a personal Ikemen GO screenpack to try and make libre assets for the engine, compatible with MUGEN 1.0 for good measure. These tracks are also used in the LibreIkemen project.

## Script Contexts

The BIFF_* scripts were also made for BLFA. They are licensed under CC0 as well, for even better reason because they are bash scripts for something trivially reproducable.

## License

The contents of this repository are under varying licenses. Licensing is to be deliberately mentioned and explained across the works found. Full texts of the legal codes of each license seen within the repository, **excluding non-free works,** will be included in the root.
