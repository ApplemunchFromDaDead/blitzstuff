#/bin/sh

# Freedoom autobuild autodownloader by blitzdoughnuts
# licensed under CC0, all rights waived, do whatever you want :)

echo Downloading autobuilds...
wget -m -q -A "zip" -nd "http://freedoom.soulsphere.org/downloads/"

echo Unzipping...
for FILE in *.zip
do
	unzip $FILE > /dev/null # stfu
done

echo Moving WADs...
for DIR in free*/
do
	cd $DIR
	mv *.wad ..
	cd ..
done

echo Cleaning...
rm *.zip
rm -rf */*
rmdir free*-*

echo Done! Enjoy your Freedoom!
