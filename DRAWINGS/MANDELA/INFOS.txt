The joke with these images is this statement:

<joke>
Fun fact: Lukifer was actually made back in 2021 along with Redmond and Bluu, all the way back in the first drawing I made of them. I had no idea what else to do with him, so I later redesigned and recycled him for LUDICRITAL. Back then, he was only known as Luke and wasn't really too much of a part of Redmond and Bluu's shenanigans, but still followed anyway for nothing better to do. Luke was rarely ever featured in the NSFW drawings, only ever seen a few times and only being involved once.

Compared to Lukifer, who is neurotic and antsy, Luke is more pragmatistic and objective. This led to his occasional use as a back-to-reality kind of character, alluding to either ridiculous cartoon (rr furry) logic and illogical or irrational statements being completely unrealistic.
</joke>

Clearly, this is false. Lukifer was a character made SPECIFICALLY for LUDICRITAL, and as a matter of fact, Redmond and Bluu were recycled and redesigned for LUDICRITAL in the form of Hamie and Clyna.
