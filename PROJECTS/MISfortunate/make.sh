# !/bin/sh
# Make script for MISfortunate
# Licensed under CC0

COMPILER="gcc"

if [ ! $1 ]; then
	echo "Please give an argument for the target frontend. (unix, x11, etc)"
	exit 1
fi

if [ $1 = "unix" ]; then
	$COMPILER main_unix.c $2 -o misfortunate
elif [ $1 = "x11" ]; then
	$COMPILER main_x11.c -lX11 $2 -o mixfortunate_x11
elif [ $1 = "love" ]; then # lol
	echo "If you're looking for horny quotes, try actually compiling the damn thing."
fi
