#ifndef MIS_NODEFAULT
#define MIS_QUOTES_DEFAULT
#endif

#ifdef MIS_ALLQUOTES
#define MIS_QUOTES_DEFAULT
#define MIS_QUOTES_FUNNY
#define MIS_QUOTES_PROGRAMMING
#define MIS_QUOTES_LRS
#define MIS_QUOTES_LIBREGAMING
#endif

const char *quotes[] = {
	#ifdef MIS_QUOTES_DEFAULT      // Original quotes, some inspired by external sources.
	"Perfection is the bane of quality.",
	"True good will always look different than fake good.",
	"In hindsight, avoidance of the everyman is wise.",
	"One day, you may too be less retarded.",
	"Incoherent blabbering is indistinguishable from spaghetti con carne.",
	"There is no difference between fascists and their self-proclaimed fighters.",
	"Something good may happen today. Who knows where, but something good may happen.",
	"Sharing is caring, yet the teachers of this seem to say otherwise. Keep on sharing.",
	"Love isn't just admiration, it is genuine care and altruism.",
	"Information wants to be free, so uncage it.",
	"There's no such thing as a free product. Three guesses as to why.",
	"When it rains, it brings a reminder of nature to the cities. The cities too devolved in their own hell to notice. That's why only the most enlightened ideas brighten in the pelting raindrops.",
	"No amount of anger is competent against a modest bit of awareness.",
	"Bad ideas are reasonably criticized. Good ideas are praised. Great ideas are scorned in confusion.",
	#endif
	#ifdef MIS_QUOTES_FUNNY       // Humorous quotes for the crass and jaded
	"You want an opinion? That's kinda gay.",
	"Remember, you're not as pathetic as others believe.",
	"Cheese is good for you.",
	"Only pussies need a separate package for offensive fortunes.",
	"The world's gone to shit, so why not compost it to make something better?",
	"Great grates make shreds for crates.",
	"Ironically, being told to go outside is an insult, despite the aforementioned outside being more hazardous in a ton of ways.",
	"Remember to rice DWM, fags!",
	"A black miner and a coal-coated white miner are both pretty dark. They're both my diggas.", // lel
	"Crimes against economy are merely unethically ethical acts.",
	"Nonfree stuff should be legally punishable by the left penalty.",
	#endif
	#ifdef MIS_QUOTES_PROGRAMMING // Some nice quotes for the keyboard warriors. Not Twitter users, I mean programers.
	"Segmentation fauxlt", // probably won't fool nobody but it'll be pretty silly
	"The Rust language is covered in a rust of bullshit. Names really do matter after all!",
	"What would you call a feminist that amends to a better ideology? A bitchift.",
	"Would you spare a 0x0F?",
	"Truly ingenious code is beyond black magic. It's hypothesis.",
	"In case of emergency, return.",
	#endif
	#ifdef MIS_QUOTES_LRS         // Quote rambunctuous quotes for a less retarded sorcerer
	"Wanna see something funny? Just turn on a TV and you'll see one every ad break.",
	"\"SJWs say the term [SJW] is offensive. We say it isn't offensive enough :D\"\n- Miloslav Ciz",
	"Remember: follow ideas, not people.",
	"Reject capitalism. Refuse competition. Let's make a Less Retarded Society.",
	#endif
	#ifdef MIS_QUOTES_LIBREGAMES  // Quotes that will rail you so hard you'll be screaming PUTA PUTA PUTA PUTA
	"To frag, or not to frag. That is the question.",
	"Why the hell doesn't this server let me join with Freedoom!?",
	"Rail a nub, he'll have salt for a day. Give a nub a railgun, he'll rival the Pacific Ocean.",
	"If you're doing bad in CTF today, remember that there's always tomorrow, and there's also always another one coming for your base.",
	"Blow off some steam not with frags, but with fun.", // very valuable
	"The coffee. Grab it. You'll need it for those pickup matches.",
	"Anyone fancy some Anarch co-op?",
	#endif
};
