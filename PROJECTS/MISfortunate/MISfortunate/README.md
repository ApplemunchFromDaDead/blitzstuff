# MISfortunate

MISfortunate is a *minimalist, insignificant suckless* fortune program that gives a random quote or fortune from a predefined list. It's different from the GNU or UNIX fortune toy in the following ways:
- Minimal I/O; fortunes aren't read from a file, but compiled into the program. Additional quotes are distributed as patches.
- Completely public domain under the Creative Commons 0 license. This includes all code, quotes, and other things in the repository.
- Aims to be as performant as possible (probably not saying much since the regular fortune program is already quite small)
