// MISfortunate X11 frontend
// For some reason, text won't draw

#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>

#include "quotes.h"

void main() {
	Display *xdisplay = XOpenDisplay(0);
	if (!xdisplay) {
		puts("MISfortunate: Can't open X display");
		return;
	};

	int scr = DefaultScreen(xdisplay);

	Window win = XCreateSimpleWindow(xdisplay, RootWindow(xdisplay, scr), 10, 10, 640, 480, 1, BlackPixel(xdisplay, scr), WhitePixel(xdisplay, scr));

	XMapWindow(xdisplay, win);

	XStoreName(xdisplay, win, "MISfortunate (X11)");

	GC graphics = DefaultGC(xdisplay, scr);

	uint8_t running = 1;
	XEvent e;
	while (running) {
		XNextEvent(xdisplay, &e);
		//if (e.type == Expose) {
		XSetForeground(xdisplay, graphics, BlackPixel(xdisplay, scr));

			XDrawString(xdisplay, win, graphics, 24, 24, quotes[0], sizeof(quotes[0]));
			XFillRectangle(xdisplay, win, graphics, 64, 128, 64, 64);
		//};
	};

	XCloseDisplay(xdisplay);
};
