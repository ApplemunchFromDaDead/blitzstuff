// MISfortunate *nix frontend

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#include "quotes.h"

void main()
{
	unsigned int quoteNum = 0;
	while (quotes[quoteNum]) quoteNum++;
	srand(time(0));
	puts(quotes[rand() % quoteNum]);
};
