# ANARCHIC BRAWLER (working title)

Anarchic Brawler is a public-domain, FOSS fighting game and fighting game engine mainly inspired by MUGEN and Ikemen Go.
**10/12/2024 NOTE: This is an outdated version! The remade version can be found [here](https://codeberg.org/blitzdoughnuts/AnarchicBrawl).**

## TO-DO:
- Fix Lukifer's drawing causing a crash
- Make all common states:
    - Walking
    - Jumping
    - Get hit standing
    - Get hit crouching
    - Get hit airborne
