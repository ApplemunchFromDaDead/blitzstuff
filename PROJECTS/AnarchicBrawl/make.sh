# !/bin/sh
# Make script for Wake to Hell, includes frontends and packaging
# Licensed under CC0

COMPILER="gcc"

if [ ! $1 ]; then
	echo "Please give an argument for the target frontend. (sdl, saf, etc)"
	exit 1
fi

if [ $1 = "sdl" ]; then
	$COMPILER main_sdl.c -lSDL2 -lSDL2_mixer -lSDL2_image $2 -o AnarchicBrawler
elif [ $1 = "saf" ]; then
	echo "It'd be impressive if this game ran on SAF."
elif [ $1 = "love" ]; then # lol
	echo "Cannot make love. Check if you have the correct characters included, and try again. Or don't."
fi
