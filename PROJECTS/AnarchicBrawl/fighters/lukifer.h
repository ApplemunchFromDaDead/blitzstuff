// Fighter code for Lukifer Dredd

#ifndef FIGHTER_INDEX_LUKIFER
#error PLease define FIGHTER_INDEX_LUKIFER before you #include lukifer.h in your game.
#endif

#define FIGHTER_NAME_LUKIFER "Lukifer Dredd"

// convenience defines
#define inputIsDown(key, index) (REF->keyhistory[index] & key)

// Load assets for the fighter
void lukiferFighterLoad() {
	printf("Loading fighter Lukifer Dredd\n");
	loadFighterSprite("sprites/lukifer/0-0.png", FIGHTER_INDEX_LUKIFER, FSTATE_STANDING, 2);
	loadFighterSprite("sprites/lukifer/1-0.png", FIGHTER_INDEX_LUKIFER, FSTATE_WALKINGL, 6);
};

// Manage fighter inputs, like commands
void lukiferFighterManageInputs(ANB_Fighter *REF) {
	uint16_t forward = (REF->flags & FFLAG_OTHERDIR) ? ANB_KEY_LEFT : ANB_KEY_RIGHT;
	uint16_t backward = (REF->flags & FFLAG_OTHERDIR) ? ANB_KEY_RIGHT : ANB_KEY_LEFT;
	
	if (REF->state == FSTATE_STANDING) {
		if (REF->keyhistory[0]) {
			if (inputIsDown(ANB_KEY_LEFT, 0)) REF->state = FSTATE_WALKINGL;
			if (inputIsDown(ANB_KEY_RIGHT, 0)) REF->state = FSTATE_WALKINGR;
		};
	};
	if (REF->state == FSTATE_WALKINGL || REF->state == FSTATE_WALKINGR)
		if (!inputIsDown(forward, 0)) REF->state = FSTATE_STANDING;
};

// Initialization for fighter, you can set variables here
void lukiferFighterStart(ANB_Fighter *REF) {
	REF->footLevel = 150;
	COMMONChangeSprite(0, REF);
};

// AT code for the fighter, ideally you should use AI variables
void lukiferFighterAI(ANB_Fighter *REF) {

};

// Per-frame code for the fighter
void lukiferFighterStep(ANB_Fighter *REF) {
	printf("Executing Lukifer step\n");
	switch (REF->state)
	{
		case FSTATE_STANDING:
			COMMONChangeSprite(FSTATE_STANDING, REF);
			switch (REF->animtime)
			{
				case 5: REF->animframe++; break;
				case 11: REF->animframe = 0; REF->animtime = 0; break;
			};
			REF->hsp = 0;
			break;
		case FSTATE_WALKINGL:
		case FSTATE_WALKINGR:
			COMMONChangeSprite(FSTATE_WALKINGL, REF);
			switch (REF->animtime)
			{
				case 5:
					// play footstep sound
				case 11:
				case 17:
				case 23:
				case 29:
					REF->animframe++; break;
				case 35: REF->animframe = 0; REF->animtime = 0; break;
			};
			switch (REF->state)
			{
				case FSTATE_WALKINGL:
					REF->hsp = -6;
					if (!(REF->flags & FFLAG_OTHERDIR)) REF->flags ^= FFLAG_OTHERDIR;
					break;
				case FSTATE_WALKINGR:
					REF->hsp = 6;
					if (REF->flags & FFLAG_OTHERDIR) REF->flags ^= FFLAG_OTHERDIR;
					break;
			};
			break;
	}
	
	// basic physics
	
	if (REF->y < stage.groundlevel + REF->footLevel) REF->vsp += 1;
	if (REF->y > stage.groundlevel + REF->footLevel) REF->y = 40;
	
	REF->x += REF->hsp;
	REF->y += REF->vsp;
	REF->animtime++;
	//drawFighter(FIGHTER_INDEX_LUKIFER, REF->animindex, REF);
};
