// Common fighter code for Anarchic Brawl. Includes walking,
// jumping, blocking, and hit states.

// Set fighter's sprite index
void COMMONChangeSprite(uint8_t spr, ANB_Fighter *REF) {
	if (REF->animindex == spr) return;
	REF->animindex = spr;
	REF->animtime = 0;
	REF->animframe = 0;
};

