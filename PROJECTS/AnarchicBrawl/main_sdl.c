/* MAIN_SDL
SDL frontend for the game. Requires SDL2, obviously.
*/

#define MAXSPRITES 1024
#define MAXSFX 128

#define GAME_WIDTH 640
#define GAME_HEIGHT 480

#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_image.h>

#undef main // TinyC doesn't like SDL's main

#include "game.h"

#include "constants.h"

SDL_Rect dspdest_rect; // this is the DESTINATION, aka screen position
SDL_Rect fade_rect; // used for fading in/out
SDL_Rect rect_draw;

SDL_Point zeropoint = {0,0};

SDL_Window *win;
SDL_Renderer *render;

SDL_Texture *font;

SDL_Texture *sprites[MAXSPRITES];

SDL_Texture *fightersprites[ANB_FIGHTER_COUNT][MAXSPRITES];
uint16_t fighterspritesDuration[ANB_FIGHTER_COUNT][MAXSPRITES];

SDL_Texture *stagesprites[MAXSPRITES];

Mix_Music *music[3];

Mix_Chunk *sfx[MAXSFX];

Mix_Chunk *fightersfx[ANB_FIGHTER_COUNT][MAXSFX];

uint8_t *keystates;

void drawSprite(uint16_t spr, int16_t x, int16_t y) {
	dspdest_rect = (SDL_Rect){x, y, 0, 0};
	SDL_QueryTexture(sprites[spr], NULL, NULL, &dspdest_rect.w, &dspdest_rect.h);
	SDL_RenderCopy(render, sprites[spr], NULL, &dspdest_rect);
};

void drawRepeatingSprite(SDL_Texture *sprite, int x, int y) {
	int _x = x;
	if (x == y) {
		dspdest_rect.x = dspdest_rect.y = x;
	} else {
		dspdest_rect.x = x;
		dspdest_rect.y = y;
	}
	SDL_QueryTexture(sprite, NULL, NULL, &dspdest_rect.w, &dspdest_rect.h);
	uint8_t limit = 0;
	for (; _x < GAME_WIDTH && limit <= 30; limit++) {
		if (_x < (-GAME_WIDTH << 2)) {
			_x += dspdest_rect.w;
			continue;
		};
		SDL_RenderCopy(render, sprite, NULL, &dspdest_rect);
		_x += dspdest_rect.w;
		dspdest_rect.x = _x;
	};
	limit = 0;
	_x = x;
	for (; _x > -dspdest_rect.w && limit <= 30; limit++) {
		SDL_RenderCopy(render, sprite, NULL, &dspdest_rect);
		_x -= dspdest_rect.w;
		dspdest_rect.x = _x;
	};
};

void drawSpriteSheeted(uint16_t spr, int x, int y, int frame, int x_sprdist, int y_sprdist, uint8_t flipped) { // supports only purely horizontal sheets
	if (x < -x_sprdist || x > x_sprdist + GAME_WIDTH) return;
	SDL_Rect destrect = {x, y, x_sprdist, y_sprdist};
	dspdest_rect.x = x_sprdist * frame;
	dspdest_rect.y = 0;
	dspdest_rect.w = x_sprdist,
	dspdest_rect.h = y_sprdist,
	SDL_RenderCopyEx(render, sprites[spr], &dspdest_rect, &destrect, 0, &zeropoint, flipped ? SDL_FLIP_HORIZONTAL : SDL_FLIP_NONE);
};

void drawImageSheeted(SDL_Texture *sprite, int x, int y, int frame, int x_sprdist, int y_sprdist, uint8_t flipped) { // supports only purely horizontal sheets
	if (x < -x_sprdist || x > x_sprdist + GAME_WIDTH) return;
	SDL_Rect destrect = {x, y, x_sprdist, y_sprdist};
	dspdest_rect.x = x_sprdist * frame;
	dspdest_rect.y = 0;
	dspdest_rect.w = x_sprdist,
	dspdest_rect.h = y_sprdist,
	SDL_RenderCopyEx(render, sprite, &dspdest_rect, &destrect, 0, &zeropoint, flipped ? SDL_FLIP_HORIZONTAL : SDL_FLIP_NONE);
};

void drawColBox(ANB_Colbox *boxin, uint8_t useCam) {
    rect_draw.x = boxin->x;
    rect_draw.y = boxin->y;
    rect_draw.w = boxin->w;
    rect_draw.h = boxin->h;
    if (useCam) {
    	rect_draw.x -= cam_x;
    	rect_draw.y -= cam_y;
    };
    SDL_RenderDrawRect(render, &rect_draw);
};

void drawTextString(const char *stuff, int x, int y) {
	// to-do: not this shit.
	// I should get to optimizing this
	SDL_SetTextureColorMod(sprites[0], 255, 255, 255);
	uint16_t _x = x;
    for (uint8_t i = 0; i < 255; i++) {
        if (stuff[i] == '\0') break; // terminator character? then get outta there before stuff gets nasty
        drawImageSheeted(font, _x, y, stuff[i]- 32, 10, 10, SDL_FLIP_NONE);
        _x += 10;
    };
};

void drawTextChar(char stuff, int x, int y) {
	// to-do: not this shit.
	// I should get to optimizing this
	SDL_SetTextureColorMod(sprites[0], 255, 255, 255);
    drawImageSheeted(font, x, y, stuff - 32, 10, 10, SDL_FLIP_NONE);
};

void drawNumber(int16_t x, int16_t y, int number) {

	char text[7];

	text[6] = 0; // terminate the string

	int8_t positive = 1;

	if (number < 0)
	{
		positive = 0;
		number *= -1;
	}

	int8_t position = 5;

	while (1)
	{
		text[position] = '0' + number % 10;
		number /= 10;

		position--;

		if (number == 0 || position == 0)
		  break;

		if (!positive)
		{
			text[position] = '-';
			position--;
		}
	}

	drawTextString(text, x, y);

	return 5 - position;
};

uint8_t isKeyPressed(uint16_t key) {
	return (inputkeys & key) && (!(lastkeys & key));
};

uint8_t isKeyDown(uint16_t key)
{
	return (inputkeys & key);
};

uint16_t loadFighterSprite(const char *thing, uint8_t fighter, uint16_t index, uint16_t duration) {
	fightersprites[fighter][index] = IMG_LoadTexture(render, thing);
	fighterspritesDuration[fighter][index] = duration;
	printf("LOADED SPRITE:\nFighter: %i\nSPR Index: %i\nDuration: %i\n", fighter, index, duration);
};

void drawFighter(uint8_t type, uint16_t spr, ANB_Fighter *REF) {
	printf("Drawing fighter of type %i with sprite %i\n", type, spr);
	int w, h;
	SDL_QueryTexture(fightersprites[type][spr], NULL, NULL, &w, &h);
	drawImageSheeted(fightersprites[type][spr], REF->x- cam_x, REF->y - cam_y, REF->animframe, w / fighterspritesDuration[type][spr], h, (REF->flags & FFLAG_OTHERDIR));
	SDL_SetRenderDrawColor(render, 0, 0x01, 0x00, 255);
	drawColBox(&REF->colboxes[0], 0);
	SDL_SetRenderDrawColor(render, 0, 0x01, 0x90, 255);
	printf("Frame: %i\nX: %i\nY: %i\nFrame width:%i\n", REF->animframe, REF->x, REF->y, w);
};

int main() {
	//if (SDL_Init(SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_EVENTS | MIX_INIT_MID | SDL_INIT_JOYSTICK) != 0) return 1;
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) return 1;
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 1, 2048) != 0)
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_WARNING, "No audio!", "The game couldn't find a usable audio system. Check your audio devices!", NULL);
	
	win = SDL_CreateWindow("Anarchic Brawl", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, GAME_WIDTH << 1, GAME_HEIGHT << 1, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
	render = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	
	SDL_RenderSetLogicalSize(render, GAME_WIDTH, GAME_HEIGHT);
	
	SDL_SetRenderDrawBlendMode(render, SDL_BLENDMODE_NONE);
	
	//winsurf = SDL_GetWindowSurface(win);
	
	//if ((SDL_NumJoysticks() > 0)) controllerinput = SDL_JoystickOpen(0);
	
	font = IMG_LoadTexture(render, "font.png");
	
	music[0] = Mix_LoadMUS("title.mid");
	Mix_PlayMusic(music[0], -1);

	SDL_SetRenderDrawColor(render, 0, 0x01, 0x90, 255);
	keystates = SDL_GetKeyboardState(NULL); // this was called every time keys() is ran, but apparently it's only needed to be called ONCE :)
	
	uint8_t running = 1;
	SDL_Event event;
	//start();
	while (running) {
		while (SDL_PollEvent(&event)) {
			switch (event.type)
			{
				case SDL_QUIT: running = 0; break;
			}
		}
		if (keystates[SDL_SCANCODE_UP]) inputkeys ^= ANB_KEY_UP;
		if (keystates[SDL_SCANCODE_DOWN]) inputkeys ^= ANB_KEY_DOWN;
		if (keystates[SDL_SCANCODE_LEFT]) inputkeys ^= ANB_KEY_LEFT;
		if (keystates[SDL_SCANCODE_RIGHT]) inputkeys ^= ANB_KEY_RIGHT;
		if (keystates[SDL_SCANCODE_A]) inputkeys ^= ANB_KEY_A;
		if (keystates[SDL_SCANCODE_S]) inputkeys ^= ANB_KEY_B;
		if (keystates[SDL_SCANCODE_D]) inputkeys ^= ANB_KEY_C;
		if (keystates[SDL_SCANCODE_Z]) inputkeys ^= ANB_KEY_X;
		if (keystates[SDL_SCANCODE_X]) inputkeys ^= ANB_KEY_Y;
		if (keystates[SDL_SCANCODE_C]) inputkeys ^= ANB_KEY_Z;
		if (keystates[SDL_SCANCODE_RETURN]) inputkeys ^= ANB_KEY_START;
		step();
		SDL_RenderPresent(render);
		SDL_RenderClear(render);
		SDL_Delay(16.667f);
	}
	
	SDL_DestroyRenderer(render);
	SDL_DestroyWindow(win);
	SDL_Quit();
	
	return 0;
};
