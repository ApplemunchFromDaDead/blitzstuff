// Game code for Anarchic Brawler

#define ANB_PRECISION 16

#define ANB_KEY_UP 1
#define ANB_KEY_DOWN 1 << 1
#define ANB_KEY_LEFT 1 << 2
#define ANB_KEY_RIGHT 1 << 3
#define ANB_KEY_A 1 << 4
#define ANB_KEY_B 1 << 5
#define ANB_KEY_C 1 << 6
#define ANB_KEY_X 1 << 7
#define ANB_KEY_Y 1 << 8
#define ANB_KEY_Z 1 << 9
#define ANB_KEY_START 1 << 10

#ifndef ANB_FPS
#define ANB_FPS 60
#endif

// FIGHTER FLAGS (FFLAG)
#define FFLAG_ACTIVE 1
#define FFLAG_CTRL 1 << 1
#define FFLAG_USEOTHERSTATE 1 << 2
#define FFLAG_OTHERDIR 1 << 3
#define FFLAG_HITUP 1 << 4 // for uppercuts, useless outside of non-hit states
#define FFLAG_DEAD 1 << 7

// COLBOX FLAGS (CFLAG)
#define CFLAG_ACTIVE 1
#define CFLAG_SPECIALATK 1 << 3
#define CFLAG_HYPERATK 1 << 4

// FIGHTER STATES (FSTATE)
// Common states for fighters to reference
// Special or otherwise unique attacks are defined by the fighters themselves, outside of these common functions
// These state numbers also work as sprite references for drawing fighters
// NOTE that fighters don't have to follow these, it's more like guidelines

#define FSTATE_STANDING 0
#define FSTATE_WALKINGL 1
#define FSTATE_WALKINGR 2
#define FSTATE_JUMPSTART 3
#define FSTATE_JUMP 4
#define FSTATE_JUMPEND 5
#define FSTATE_CROUCH 6
// attacks
#define FSTATE_LPUNCH 10
#define FSTATE_MPUNCH 11
#define FSTATE_HPUNCH 12
#define FSTATE_LKICK 13
#define FSTATE_MKICK 14
#define FSTATE_HKICK 15
// crouch attacks
#define FSTATE_CLPUNCH 16
#define FSTATE_CMPUNCH 17
#define FSTATE_CHPUNCH 18
#define FSTATE_CLKICK 19
#define FSTATE_CMKICK 20
#define FSTATE_CHKICK 21

#define FSTATE_HURTSTAND 30
#define FSTATE_HURTCROUCH 31
#define FSTATE_HURTAIR 32

int16_t cam_x;
int16_t cam_y;

uint16_t inputkeys, lastkeys;

struct {
	uint8_t state, time, subtime;
} game;

struct {
	uint8_t select, index;
} menu;

typedef struct {
	int16_t x, y, w, h;
	uint8_t flags;
	uint8_t owner; // owner of hitbox; add 4 if on P2's side
} ANB_Colbox;

typedef struct {
	int16_t x, y, w, h;
	uint8_t flags;
	uint8_t owner; // owner of hitbox; add 4 if on P2's side
	int16_t ownerXVel, ownerYVel, victimXVel, victimYVel;
	int16_t dmg, ownerStuntime, victimStuntime;
} ANB_Hitbox;

struct {
	int16_t groundlevel;
	uint8_t layers;
	uint8_t layerData[16][2]; // index 0 of 2nd array = stage sprite, index 2 = flags and stuff
} stage;

typedef struct {
	int16_t x, y, hsp, vsp;
	uint16_t state, animtime, animframe, animindex, footLevel;
	int16_t vars[32];
	uint16_t stuntine;
	uint8_t aiLevel, flags, index;
	ANB_Colbox colboxes[16];
	ANB_Hitbox hitboxes[16];
	uint16_t keys, keyhistory[30];
} ANB_Fighter;

ANB_Fighter plrs[2][4]; // 2 teams of 4 fighters each

static uint8_t checkBoxCollision(ANB_Colbox *box1, ANB_Colbox *box2) {
    return (!(
    	box2->x > box1->x + box1->w ||
    	box2->x + box2->w < box1->x ||
    	box2->y > box1->y + box1->h ||
    	box2->y + box2->h < box1->y
    ));
};

static uint8_t checkPointCollision(int px, int py, ANB_Colbox *box) {
    return ( ( px > box->x && px < box->x + box->w)
    && ( py > box->y && py < box->y + box->h ) );
};

uint8_t isKeyPressed(uint16_t key);
uint8_t isKeyHeld(uint16_t key);

void drawFighter(uint8_t type, uint16_t spr, ANB_Fighter *REF);				// draw a fighter
void drawFighterSprite(uint8_t type, uint16_t spr, int16_t x, int16_t y);			// draw a sprite that belongs to a fighter (e.g. pop-up text)
void drawSprite(uint16_t spr, int16_t x, int16_t y);								// draw a general sprite (e.g. hit effects)

uint16_t loadImageExternal(const char *thing, uint16_t index);				// load in an external image from a path, return the new index of the image

uint16_t loadFighterSprite(const char *thing, uint8_t fighter, uint16_t index, uint16_t duration);				// load an external image to a fighter's sprite list (fightersprites[fighter][index])
uint8_t loadStageSprite(const char *thing); // load an external image to a stage sprite list, return index

void drawTextString(const char *stuff, int x, int y);

void fighterResetColboxes(ANB_Fighter *REF) {
	for (uint8_t i; i < 16; i++) {
		REF->colboxes[i].flags = 0;
	};
};

// find horizontal distance between closest enemy
int16_t fighterGetP2Distance(ANB_Fighter *REF) {
	// loop through all enemies and return closest distance
};

// this gives me a fucking panic attack
int16_t fighterCheckAttacked(ANB_Fighter *REF, uint8_t side) {
	for (uint8_t i; i < 4; i++) {
		for (uint8_t mybox; mybox < 16; mybox++) {
			if (!(REF->colboxes[mybox].flags & CFLAG_ACTIVE)) continue;
			for (uint8_t theirbox; theirbox < 16; theirbox++) {
				if (checkBoxCollision(&REF->colboxes[mybox], &plrs[side][i].hitboxes[theirbox])) {
					REF->state = FSTATE_HURTSTAND;
					REF->hsp = plrs[side][i].hitboxes[theirbox].victimXVel;
					REF->vsp = plrs[side][i].hitboxes[theirbox].victimYVel;
				};
			};
		};
	};
};

// ------ LOAD ORDER MATTERS! ------
// Fighters come with a fighter index; you must define it before you include their code
#include "fighters/COMMON.h" // COMMON FIGHTER CODE with basic states

#define FIGHTER_INDEX_LUKIFER 0

#include "fighters/lukifer.h"

void fight_start() {
	uint8_t fightersLoaded[1][1];
	for (uint8_t i; i < 4; i++) {
		if (!(plrs[0][i].flags & FFLAG_USEOTHERSTATE) && !fightersLoaded[plrs[0][i].index][0]) {
			switch (plrs[0][i].index)
			{
				case FIGHTER_INDEX_LUKIFER:
					lukiferFighterLoad();
					lukiferFighterStart(&plrs[0][i]);
					break;
			};
			fightersLoaded[plrs[0][i].index][0] = 1;
		};
		if (!(plrs[1][i].flags & FFLAG_USEOTHERSTATE) && !fightersLoaded[plrs[1][i].index][0]) {
			switch (plrs[1][i].index)
			{
				case FIGHTER_INDEX_LUKIFER:
					lukiferFighterLoad();
					lukiferFighterStart(&plrs[1][i]);
					break;
			};
		};
		fightersLoaded[plrs[1][i].index][0] = 1;
	};
};

void fight_step() {
	// execute fighter code
	for (uint8_t i = 0; i < 4; i++) {
		if (plrs[0][i].flags & FFLAG_ACTIVE) {
			// run basic stuff
			printf("Player %i on team 0 is doing step...", i);
			fighterCheckAttacked(&plrs[0][i], 1);
			switch (plrs[0][i].index)
			{
				case FIGHTER_INDEX_LUKIFER:
					lukiferFighterStep(&plrs[0][i]);
					if (plrs[0][i].flags & FFLAG_CTRL)
						lukiferFighterManageInputs(&plrs[0][i]);
					break;
			};
			for (uint8_t ii = 0; ii < 29; ii++) {
				plrs[0][i].keyhistory[ii + 1] = plrs[0][i].keyhistory[ii];
				plrs[0][i].keyhistory[ii] = plrs[0][i].keys;
			};
		};
		if (plrs[1][i].flags & FFLAG_ACTIVE) {
			printf("Player %i on team 1 is doing step...", i);
			switch (plrs[1][i].index)
			{
				case FIGHTER_INDEX_LUKIFER:
					lukiferFighterStep(&plrs[1][i]);
					if (plrs[1][i].flags & FFLAG_CTRL)
						lukiferFighterManageInputs(&plrs[1][i]);
					break;
			};
			for (uint8_t ii = 0; ii < 29; ii++) {
				plrs[1][i].keyhistory[ii + 1] = plrs[1][i].keyhistory[ii];
				plrs[1][i].keyhistory[ii] = plrs[1][i].keys;
			};
		};
	};
};

void start() {

};

void step() {
	switch (game.state)
	{
		case 0: // main menu
			drawTextString("ANARCHIC BRAWLER", 0, 0);
			drawTextString("Really Fucking Beta", 0, 10);
			drawTextString("PLAY", 20, 60);
			drawTextString("OPTIONS", 20, 70);
			drawTextString("EXIT", 20, 80);
			drawTextString(">", 10, 60 + (10 * menu.select));
			
			if (isKeyPressed(ANB_KEY_UP)) menu.select--;
			if (isKeyPressed(ANB_KEY_DOWN)) menu.select++;
			if (isKeyPressed(ANB_KEY_X)) {
				if (menu.select == 0) {
					plrs[0][0].flags ^= FFLAG_ACTIVE | FFLAG_CTRL;
					fight_start();
					game.state = 1;
				};
			};
			break;
		case 1:
			fight_step();
			game.subtime++;
			if (game.subtime == ANB_FPS){
				game.subtime = 0;
				game.time--;
			};
			plrs[0][0].keys = inputkeys;
			break;
	};
	lastkeys = inputkeys;
	inputkeys = 0;
};
