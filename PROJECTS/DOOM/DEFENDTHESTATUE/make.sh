#!/bin/sh

# you may want to change this
ACC="/home/blitzdoughnuts/Documents/acc/acc"

if [ ! $1 ]; then
	echo "Please supply an argument (acs, package)"
	exit 1
fi

if [ $1 = "acs" ]; then
	$ACC BEHAVIOR.txt && mv BEHAVIOR.o acs/
	$ACC DTS01.txt && mv DTS01.o acs/
elif [ $1 = "package" ]; then
	mkdir BUILD
	cp -r maps acs filter music textures graphics BUILD
	cp *.txt BUILD
	mv BUILD/README_WAD.txt BUILD/README.txt
fi
