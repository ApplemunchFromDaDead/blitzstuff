damagetype "Player" {} // damage dealt by players
damagetype "Repair" {} // Fist-specific, deals damage to only enemies, repairs statue

actor DTSPlayerPuff : BulletPuff
{
  DamageType "Player"
}

actor DTSRepairPuff : BulletPuff
{
  DamageType "Repair"
}

actor DTSFist : Fist replaces Fist
{
  DamageType "Repair"
}

actor Credits : Inventory {
  Inventory.MaxAmount 5000
  Inventory.PickupMessage "Picked up some Credits."
  Inventory.Amount 5
  Translation "160:167=112:119"
  States {
    Spawn:
      CLIP A -1 Bright
      Stop
  }
}

// Make every monster drop credits, and ONLY credits

actor DTSPistolZomb : Zombieman replaces Zombieman
{
  DropItem "Credits"
}

actor DTSShottyZomb : ShotgunGuy replaces ShotgunGuy
{
  DropItem "Credits"
}

actor DTSSerpent : DoomImp replaces DoomImp
{
  DropItem "Credits"
}

actor DTSMiniZomb : ChaingunGuy replaces ChaingunGuy
{
  DropItem "Credits", 255, 10
}

actor DTSWorm : Demon replaces Demon
{
  DropItem "Credits", 255, 15
}

actor DTSSpookyWorm : Spectre replaces Spectre
{
  DropItem "Credits", 255, 15
}

actor DTSOctaminator : Revenant replaces Revenant
{
  DropItem "Credits", 255, 50
}

actor DTSSlug : Fatso replaces Fatso
{
  DropItem "Credits", 255, 50
}

actor DTSTechnospider : Arachnotron replaces Arachnotron
{
  DropItem "Credits", 255, 60
}

// Infinite ammo weps, plus some changes

actor DTSPistol : Pistol replaces Pistol
{
  DamageType "Player"
  Weapon.AmmoUse 0
  Weapon.AmmoType "None"
}

actor DTSShotty : Shotgun replaces Shotgun
{
  DamageType "Player"
  Weapon.AmmoUse 0
  Weapon.AmmoType "None"
}

actor DTSDBShotty : SuperShotgun replaces SuperShotgun
{
  DamageType "Player"
  Weapon.AmmoUse 0
  Weapon.AmmoType "None"
}

actor DTSMinigun : Chaingun replaces Chaingun
{
  DamageType "Player"
  Weapon.AmmoUse 0
  Weapon.AmmoType "None"
}

actor DTSMissileLauncher : RocketLauncher replaces RocketLauncher
{
  DamageType "Player"
  Weapon.AmmoUse 0
  Weapon.AmmoType "None"
}

actor DTSPolaricCannon : PlasmaRifle replaces PlasmaRifle
{
  DamageType "Player"
  Weapon.AmmoUse 0
  Weapon.AmmoType "None"
}

actor DTSSKAG : BFG9000 replaces BFG9000
{
  DamageType "Player"
  Weapon.AmmoUse 0
  Weapon.AmmoType "None"
}

actor DTSPlayer : DoomPlayer replaces DoomPlayer
{
  Player.DisplayName "Statue Defender"
  Player.StartItem "DTSPistol"
  Player.StartItem "DTSFist"
  Player.WeaponSlot 1, "DTSFist", "Chainsaw"
  Player.WeaponSlot 2, "DTSPistol"
  Player.WeaponSlot 3, "DTSDBShotty", "DTSShotty"
  Player.WeaponSlot 4, "DTSMinigun"
  Player.WeaponSlot 5, "DTSMissileLauncher"
  Player.WeaponSlot 6, "DTSPolaricCannon"
  Player.WeaponSlot 7, "DTSSKAG"
}

actor DTSStatue : Actor 31900
{
  +ISMONSTER
  Monster
  +SOLID
  +FRIENDLY
  Health 10000
  DamageFactor "Repair", -1.0
  DamageFactor "Player", 0.0
  Height 100
  Radius 32
  Mass 9001
  Translation "0:255=%[0,0,0]:[1.6,1.6,0.2]"
  States {
    Spawn:
      CYBR A 5 A_AlertMonsters(0, AMF_TARGETEMITTER)
      Loop
  }
}
