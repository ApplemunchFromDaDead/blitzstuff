# Eldrich Anarchy

*pacifo-anarchist, suckless-ish, anti-capitalist mod for Freedoom*

## What is this?

Eldrich Anarch (formerly Anarch x Freedoom) is a GZDoom mod that implements the Anarch arsenal into Freedoom. Any IWAD can likely be used, but non-libre ones will not be given active support. As such, it's recommended to use [Freedoom](https://freedoom.github.io) as the IWAD.

This mod blends the Anarch weapons and some of its gameplay elements into the levels of Freedoom, including:
    - A Shotgun that uses bullets
    - No BFG
    - You have all weapons, but no ammo on start.
    - Everything you shoot is a projectile. Even your bullets.

This is made with ZScript and, as such, cannot be run on Zandronum or any \*ZDoom source port that doesn't support it. A Decorate port is being considered.
