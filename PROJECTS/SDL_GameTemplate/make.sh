# !/bin/sh
# Make script for the basic SDL2 game templace, includes frontends and packaging
# Licensed under CC0

COMPILER="gcc"

if [ ! $1 ]; then
	echo "Please give an argument for the target frontend. (game or package)"
	exit 1
fi

if [ $1 = "game" ]; then
	$COMPILER main_sdl.c -lSDL2 -lSDL2_mixer -lSDL2_image $2 -o SDL2Game
elif [ $1 = "package" ]; then
	BUILDDIR="BUILD/"

	cp SDL2Game ${BUILDDIR}
	cp *.mid ${BUILDDIR}
	cp -r sfx ${BUILDDIR}
	cp -r sprites ${BUILDDIR}
	echo "The game (assuming your binary is named SDL2Game) and all required files for a *NIX build are now in the $BUILDDIR folder."
fi
