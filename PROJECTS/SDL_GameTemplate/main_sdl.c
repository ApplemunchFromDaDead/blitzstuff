// SDL2 frontend

#include <SDL2/SDL.h>
//#include <SDL2/SDL_mixer.h>
//#include <SDL2/SDL_image.h>

#undef main

#include "game.h" // main can interface with the game

//Mix_Music *music[2];
//Mix_Chunk *sfx[10];

SDL_Texture *wintext;

SDL_Texture *sprites[1];

SDL_Rect dspdest_rect; // this is the DESTINATION, aka screen position
SDL_Point zeropoint = {0,0};

SDL_Renderer *render;
SDL_Window *win;

void draw() {
    // render shit
    // check Wake to Hell for drawing functions you can slap onto here
};

int main() {
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) return 1;
	
	#define REALWINTITLE WINTITLE
	win = SDL_CreateWindow(REALWINTITLE, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
	render = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	#undef REALWINTITLE
	
	SDL_RenderSetLogicalSize(render, WIDTH, HEIGHT); // set rendering res to desired resolution
	SDL_SetRenderDrawColor(render, 0, 0x00, 0x00, 255); // tell SDL2 to blank out the window before drawing anything
	
	// load assets here; Mix_LoadWAV for sounds, IMG_LoadTexture for sprites and spritesheets
	
	uint8_t running = 1;
	SDL_Event event;
	start();
	while (running) {
		while (SDL_PollEvent(&event)) {
			switch (event.type)
			{
				case SDL_QUIT: running = 0; break;
			}
		}
		draw();
		step();
		
		SDL_RenderPresent(render);
		SDL_RenderClear(render);
		SDL_Delay(16.667f);
	}
	
	SDL_DestroyRenderer(render);
	SDL_DestroyWindow(win);
	SDL_Quit();
	
	return 0;
};
