#define WINTITLE "Starveless"

#define WIDTH 960
#define HEIGHT 540

#define KEY_LF 1
#define KEY_RT 1 << 1
#define KEY_UP 1 << 2
#define KEY_DN 1 << 3
#define KEY_INT 1 << 4 // interact with nearby things/items
#define KEY_ATK 1 << 5 // attack nearby mobs
#define KEY_ACCEPT 1 << 6 // accept in menus, alt action in game
#define KEY_CANCEL 1 << 7 // back in menus, alt action in game

#define PLR_STATE_NONE 0
#define PLR_STATE_HURT 1
#define PLR_STATE_DIGGING 2
#define PLR_STATE_PICKING 3

#define IS_ACTIVE 1

#define ITEM_STICKS 1
#define ITEM_ROCKS 2
#define ITEM_FLINT 3
#define ITEM_GRASS 4
#define ITEM_ROPE 5
#define ITEM_PLANKS 6
#define ITEM_CUTSTONE 7
#define ITEM_LOGS 8
#define ITEM_STEELRAW 9
#define ITEM_STEELBAR 10
#define ITEM_PICKAXE 11
#define ITEM_HATCHET 12
#define ITEM_SPEAR 13

#define OBJ_EVERGREEN 0
#define OBJ_ACORNTREE 1
#define OBJ_BOULDER 2
#define OBJ_STEELBOULDER 3
#define OBJ_GRASSTUFT 4

#define PFLAG_GOTOPOINT 1
#define PFLAG_CRAFTMENU 1 << 7

#ifndef DSG_RNG_MODE
#define DSG_RNG_MODE 1
#endif

typedef struct {
	int x, y, hsp, vsp, hsp_sub, vsp_xub;
	int walkpoint_x, walkpoint_y;
	uint16_t walkpoint_target, walkpoint_type;
	uint8_t flags, character, state;
	uint8_t items[10][2], equipped[3][2];
	int16_t vars[4];
	uint8_t hunger, health, sanity;
	uint8_t animframe, animflimit, animtimer;
} DSG_Player;

typedef struct {
	int x, y;
	uint8_t item, flags;
} DSG_DroppedItem;

typedef struct {
	int x, y, hsp, vsp, hsp_sub, vsp_xub;
	uint8_t flags, type;
	int16_t vars[4];
} DSG_Mob;

typedef struct {
	int x, y;
	uint8_t type, flags;
	int16_t vars[4];
} DSG_Object;

typedef struct {
	uint8_t index;
} DSG_CraftMenu;

DSG_Player plr;
DSG_DroppedItem items[512];
DSG_Mob mobs[512];
DSG_Object objs[512];
uint16_t mobs_count, objs_count, items_count;

DSG_CraftMenu cmenu;

int cam_x, cam_y;

uint8_t input_keys;

static void createItem(int x, int y, uint8_t type) {
	items[items_count].flags ^= IS_ACTIVE;
	items[items_count].x = x;
	items[items_count].y = y;
	items[items_count].item = type;
	items_count++;
	printf("Created item #%i\n", items_count - 1);
};

void playSound(uint8_t index);

void plr_sayLine(uint16_t index) {

};

int RNG_getNum();

void start() {
    // put game init code here
    objs[0].flags ^= IS_ACTIVE;
    objs[0].x = 64;
    objs[0].y = 128;
    items[0].flags ^= IS_ACTIVE;
    items[0].x = -64;
    items[0].y = 0;
    items[0].item = ITEM_STICKS;
    
    plr.equipped[0][0] = ITEM_HATCHET;
    plr.equipped[0][1] = 100;
    
    plr.animflimit = 43;
};

static void interactWithObject(DSG_Object *REF) {
	switch (REF->type)
	{
		case OBJ_EVERGREEN: case OBJ_ACORNTREE:
			if (plr.equipped[0][0] != ITEM_HATCHET) break;
			plr.state = PLR_STATE_DIGGING;
			if (!plr.vars[0]) plr.vars[0] = 30;
			plr.vars[1] = plr.walkpoint_target;
			break;
	}
};
static void interactWithItem(DSG_DroppedItem *REF) {
	if (!(REF->flags & IS_ACTIVE)) return;
	for (uint8_t i = 0; i < 9; i++) {
		if (plr.items[i][0] == REF->item) {
			if (plr.items[i][1] >= 40) continue; // stack is full!
			plr.items[i][1]++;
			REF->flags ^= IS_ACTIVE;
		playSound(0);
			printf("Stacked item of type %i into slot %i of player's inventory\n", REF->item, i);
			break;
		} else if (plr.items[i][0] != 0) continue;
		plr.items[i][0] = REF->item;
		plr.items[i][1] = 1;
		REF->flags ^= IS_ACTIVE;
		playSound(0);
		printf("Put item of type %i into slot %i of player's inventory\n", REF->item, i);
		break;
	};
};

void step() {
    // this code runs every frame; put per-frame code here like game logic
    
    switch (plr.state)
    {
    	case PLR_STATE_NONE:
    		#define WALKSPEED 4
			if (!(plr.flags & PFLAG_CRAFTMENU)) {
				if (!(plr.flags & PFLAG_GOTOPOINT)) {
					if (input_keys & KEY_UP) plr.y -= WALKSPEED;
					if (input_keys & KEY_DN) plr.y += WALKSPEED;
					if (input_keys & KEY_LF) plr.x -= WALKSPEED;
					if (input_keys & KEY_RT) plr.x += WALKSPEED;
				} else {
					if (plr.x != plr.walkpoint_x) {
						plr.x += (plr.walkpoint_x - plr.x <= -1) ? -WALKSPEED : WALKSPEED;
					};
					if (plr.y != plr.walkpoint_y) {
						plr.y += (plr.walkpoint_y - plr.y <= -1) ? -WALKSPEED : WALKSPEED;
					};
					if (plr.x - 8 <= plr.walkpoint_x && plr.x + 8 >= plr.walkpoint_x ) {
						plr.flags ^= PFLAG_GOTOPOINT;
						switch (plr.walkpoint_type)
						{
							case 0: default:
								interactWithItem(&items[plr.walkpoint_target]);
								break;
							case 1:
								interactWithObject(&objs[plr.walkpoint_target]);
								break;
						};
					};
					
					if (input_keys & KEY_INT) plr.flags ^= PFLAG_GOTOPOINT;
				};
			} else {
				if (input_keys & KEY_LF) cmenu.index--;
				if (input_keys & KEY_RT) cmenu.index++;
				
				if (input_keys & KEY_INT) {
					uint8_t craft_requirements;
					#define CRAFTREQ_HASITEMS 1
					#define CRAFTREQ_HASAMOUNT 1 << 1
					#define CRAFTREQ_INVNOTFULL 1 << 2
				};
				
			};
			#undef WALKSPEED
			break;
		case PLR_STATE_DIGGING:
			if (!(objs[plr.vars[1]].flags & IS_ACTIVE)) break;
			if (plr.vars[0]) {
				plr.vars[0]--;
				break;
			};
			plr.vars[0] = 30;
			objs[plr.vars[1]].vars[0]--;
			plr.equipped[0][1]--;
			playSound(0);
			if (objs[plr.vars[1]].vars[0] <= 0) {
				objs[plr.vars[1]].flags ^= IS_ACTIVE;
				uint8_t i = 0;
				for (; i < 2; i++) {
					createItem(objs[plr.vars[1]].x, objs[plr.vars[1]].y, ITEM_LOGS);
				};
				plr.state = PLR_STATE_NONE;
				// to-do: drop pine cones or acorns
			};
			break;
    
    
	}
	
	plr.animtimer++;
	if (plr.animtimer > 4) {
		plr.animframe++;
		if (plr.animframe > plr.animflimit) plr.animframe = 0;
	};
    
    cam_x = plr.x - (WIDTH >> 1);
    cam_y = plr.y - (HEIGHT >> 1);
    
    if (input_keys & KEY_INT) {
    	uint8_t alreadyGotThing = 0;
    	for (uint16_t i = 0; i < 512; i++) {
    		if (!(items[i].flags & IS_ACTIVE)) continue;
    		if (items[i].x + plr.x >= -200 && items[i].x + plr.x <= 200) {
    			plr.walkpoint_target = i;
    			plr.walkpoint_type = 0;
    			plr.walkpoint_x = items[i].x;
    			plr.walkpoint_y = items[i].y;
    			if (!(plr.flags & PFLAG_GOTOPOINT)) plr.flags ^= PFLAG_GOTOPOINT;
    		};
    		alreadyGotThing = 1;
    	};
    	if (!alreadyGotThing) {
			for (uint16_t i = 0; i < 512; i++) {
				if (!(objs[i].flags & IS_ACTIVE)) continue;
				if (objs[i].x + plr.x >= -50 && objs[i].x + plr.x <= 50) {
					plr.walkpoint_target = i;
					plr.walkpoint_type = 1;
					plr.walkpoint_x = objs[i].x;
					plr.walkpoint_y = objs[i].y;
					if (!(plr.flags & PFLAG_GOTOPOINT)) plr.flags ^= PFLAG_GOTOPOINT;
				};
			};
    	};
    };
    if (input_keys & KEY_ATK) {
    	// attack
    };
    
    input_keys = 0;
};
