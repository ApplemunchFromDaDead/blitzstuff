// SDL2 frontend

#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_image.h>

#include <stdlib.h>
#include <time.h>

#undef main

#include "game.h" // MAIN can interface with the game
#include "str.h" // strings for the game

Mix_Music *music[2];
Mix_Chunk *sfx[2];

SDL_Texture *wintext;

SDL_Texture *sprites[3];
SDL_Texture *itemsprites[2]; // add (size / 2) to get dropped sprites

SDL_Rect dspdest_rect; // this is the DESTINATION, aka screen position
SDL_Rect rect_draw;
SDL_Point zeropoint = {0,0};

SDL_Renderer *render;
SDL_Window *win;

uint8_t *keystates;

void drawRectangle(int16_t x, int16_t y, int16_t w, int16_t h, uint32_t color) {
	SDL_SetRenderDrawColor(render, 255, 255, 255, 255);
	rect_draw = (SDL_Rect){x,y,w,h};
	SDL_RenderDrawRect(render, &rect_draw);
};

void drawSprite(SDL_Texture *sprite, int x, int y) {
	dspdest_rect = (SDL_Rect){x, y, 0, 0};
	SDL_QueryTexture(sprite, NULL, NULL, &dspdest_rect.w, &dspdest_rect.h);
	SDL_RenderCopy(render, sprite, NULL, &dspdest_rect);
};

void drawRepeatingSprite(SDL_Texture *sprite, int x, int y, uint8_t size) { // this DOESN'T repeat yet, because I am big Stupid(TM)
	int _x = x;
	if (x == y) {
		dspdest_rect.x = dspdest_rect.y = x;
	} else {
		dspdest_rect.x = x;
		dspdest_rect.y = y;
	}
	SDL_QueryTexture(sprite, NULL, NULL, &dspdest_rect.w, &dspdest_rect.h);
	uint8_t limit = 0;
	for (; _x < WIDTH && limit <= 30; limit++) {
		if (_x < (-WIDTH << 2)) {
			_x += dspdest_rect.w;
			continue;
		};
		SDL_RenderCopy(render, sprite, NULL, &dspdest_rect);
		_x += dspdest_rect.w;
		dspdest_rect.x = _x;
	};
	_x = x - dspdest_rect.w;
	dspdest_rect.x = _x;
	SDL_RenderCopy(render, sprite, NULL, &dspdest_rect);
};

void drawSpriteSheeted(SDL_Texture *sprite, int x, int y, int frame, int x_sprdist, int y_sprdist, uint8_t flipped, uint8_t size) { // supports only purely horizontal sheets
	if (x < -x_sprdist || x > x_sprdist + WIDTH) return;
	SDL_Rect destrect = {x, y, x_sprdist * size / 128, y_sprdist * size / 128};
	dspdest_rect.x = x_sprdist * frame;
	dspdest_rect.y = 0; // this somehow fixes an issue where the sprite would vertically stretch when moving the camera vertically, but crop off within the sprite's intended bounds. what?
	dspdest_rect.w = x_sprdist,
	dspdest_rect.h = y_sprdist,
	SDL_RenderCopyEx(render, sprite, &dspdest_rect, &destrect, 0, &zeropoint, flipped ? SDL_FLIP_HORIZONTAL : SDL_FLIP_NONE);
};

void playSound(uint8_t index) {
	Mix_PlayChannel(-1, sfx[index], 0);
};

int RNG_getNum() {
	return rand();
};

static void keys() {
	#define DSGSDL_KEYDOWN(x) keystates[SDL_SCANCODE_ ## x]
	if (DSGSDL_KEYDOWN(UP)) input_keys ^= KEY_UP;
	if (DSGSDL_KEYDOWN(DOWN)) input_keys ^= KEY_DN;
	if (DSGSDL_KEYDOWN(LEFT)) input_keys ^= KEY_LF;
	if (DSGSDL_KEYDOWN(RIGHT)) input_keys ^= KEY_RT;
	if (DSGSDL_KEYDOWN(SPACE)) input_keys ^= KEY_INT;
	if (DSGSDL_KEYDOWN(F)) input_keys ^= KEY_ATK;
	if (DSGSDL_KEYDOWN(Z)) input_keys ^= KEY_ACCEPT;
	if (DSGSDL_KEYDOWN(X)) input_keys ^= KEY_CANCEL;
};

void draw() {
	drawSpriteSheeted(sprites[0], plr.x - cam_x - 80, plr.y - cam_y - 75,plr.animframe,240,240, 0, 64);
	for (uint16_t i = 0; i < 511; i++) {
		if (!(objs[i].flags & IS_ACTIVE)) continue;
		drawSprite(sprites[1], objs[i].x - cam_x, objs[i].y - cam_y / 1.5);
	};
	
	for (uint16_t i = 0; i < 511; i++) {
		if (!(items[i].flags & IS_ACTIVE)) continue;
		drawSprite(itemsprites[1], items[i].x - cam_x, items[i].y - cam_y / 1.5);
	};
	
	for (uint8_t i = 0; i < 10; i++) {
		drawRectangle(36 * (i + 1), HEIGHT - 34, 32, 32, 0);
		if (plr.items[i][1]) drawSprite(itemsprites[0], 36 * (i + 1), HEIGHT - 34);
	};
};

int main() {
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) return 1;
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 1, 2048) != 0)
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_WARNING, "No audio!", "The game couldn't find a usable audio system. Check your audio devices!", NULL);
	
	#define REALWINTITLE WINTITLE
	win = SDL_CreateWindow(REALWINTITLE, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
	render = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	#undef REALWINTITLE
	
	SDL_RenderSetLogicalSize(render, WIDTH, HEIGHT); // set rendering res to desired resolution
	SDL_SetRenderDrawColor(render, 0x20, 0x40, 0x80, 255); // tell SDL2 to blank out the window before drawing anything
	
	// load assets here; Mix_LoadWAV for sounds, IMG_LoadTexture for sprites and spritesheets
	
	sprites[0] = IMG_LoadTexture(render, "spr/PLACEHOLDERS/player.png");
	sprites[1] = IMG_LoadTexture(render, "spr/PLACEHOLDERS/evergreen.png");
	
	itemsprites[0] = IMG_LoadTexture(render, "spr/PLACEHOLDERS/item_pickaxe.png");
	itemsprites[1] = IMG_LoadTexture(render, "spr/PLACEHOLDERS/item_axe_world.png");
	
	sfx[0] = Mix_LoadWAV("sfx_pickup.wav");
	
	keystates = SDL_GetKeyboardState(NULL);
	
	uint8_t running = 1;
	SDL_Event event;
	start();
	while (running) {
		while (SDL_PollEvent(&event)) {
			switch (event.type)
			{
				case SDL_QUIT: running = 0; break;
			}
		}
		draw();
		SDL_SetRenderDrawColor(render, 0x20, 0x40, 0x80, 255);
		keys();
		step();
		
		SDL_RenderPresent(render);
		SDL_RenderClear(render);
		SDL_Delay(16.667f);
	}
	
	SDL_DestroyRenderer(render);
	SDL_DestroyWindow(win);
	SDL_Quit();
	
	return 0;
};
