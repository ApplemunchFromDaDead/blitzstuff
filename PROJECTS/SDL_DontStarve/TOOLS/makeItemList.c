#include <stdio.h>

FILE *infile;
FILE *outfile;

char infile_contents[64][512];

int countString(char str[]) {
	int count = 0;
	while (str[count] != '\0') {
		count++;
	};
	return count;
};

int main(int argc, char *argv[]) {
	if (argc <= 1) {
		puts("Starveless - Item Macro Tool");
		puts("Input a file to output a list of");
		puts("#define macros to be used in the");
		puts("game as ITEMLIST_OUT.txt");
		return 0;
	};
	if (!(infile = fopen(argv[1], "r"))) {
		puts("Can't open file! The fuck?");
		return 1;
	};
	puts("Opened infile");
	int i = 0;
	while (fgets(infile_contents[i], 64, infile)) {
		i++;
	};
	fclose(infile);
	
	printf("%s", infile_contents[0]);
	
	outfile = fopen("FILELIST_OUT.txt", "rw");
	puts("Opened outfile");
	
	i = 0;
	while (i < 2) {
		fwrite("#define ITEM_", 14, 1, outfile);
		puts("Wrote define");
		fwrite(&infile_contents[i], countString(&infile_contents), 1, outfile);
		puts("Wrote string");
		i++;
	};
	
	fclose(outfile);
	
	return 0;
};
