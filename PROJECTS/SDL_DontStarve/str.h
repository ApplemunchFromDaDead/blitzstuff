#ifndef DSG_STRINGS
#define DSG_STRINGS

const char *charlines_dan[] = {
	"Impossible.", // unable to do thing / invalid line
	"Seems good for some basic tools.", // these next lines go by item indexes
	"I always liked to keep these!",
	"Surprisingly sharp for a wild flint.",
	"Sadly dead, as it doesn't belong to be.",
	"Did anybody consider this as an option?",
	"Good wood for even better construction!",
	"These rocks are pretty brick-y.",
	"Yippee-ki-yay, deforestation...",
	"Iron? I thought I'd see this in a cave or something.",
	"It's already... a bar. This cannot be possible.",
	"Gets the rocks crushed good.",
	"Gets the trees cut good.",
	"Gets blood all over my clothes.",
	"Impressive that these ones stay through all seasons.", // object inspection
	"Acorns, I simply do not understand.",
	"It's a stone, Mother Nature. You made it.",
	"Is that metal? I think there's metal in there.",
	"I don't think that is healthy grass."
};

#endif
