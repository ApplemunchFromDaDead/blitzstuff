#/bin/sh

# drummyfish fetching script
# the ULTIMATE netstalking device :) (sorry miloslav)
# licensed under CC0, all rights waived, do whatever you want :)

makeBackup () {
	if [ -d "$1BAK" ]; then
		rm -rf "$1BAK";
	fi
	mv $1 "$1BAK"
}

echo Downloading tastyfish.cz:
wget -r --reject-regex="lrs" http://www.tastyfish.cz
echo Cloning coom.tech repos:
echo == Anarch ==
makeBackup Anarch
git clone https://git.coom.tech/drummyfish/Anarch.git
echo == SAF ==
makeBackup SAF
git clone https://git.coom.tech/drummyfish/SAF.git
echo == Comun ==
makeBackup comun
git clone https://git.coom.tech/drummyfish/comun.git
echo == Less Retarded Wiki ==
makeBackup less_retarded_wiki
git clone https://git.coom.tech/drummyfish/less_retarded_wiki.git
echo == small3dlib ==
makeBackup small3dlib
git clone https://git.coom.tech/drummyfish/small3dlib.git

echo Cleaning .git folders...
rm -rf Anarch/.git
rm -rf SAF/.git
rm -rf comun/.git
