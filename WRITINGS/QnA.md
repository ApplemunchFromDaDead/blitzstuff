This is a basic QnA about not just my stuff, but also life stuff in general, which may, could, and will (or not) reflect my views.

Use CTRL+F or whatever Find feature you have to sift through questions.

# LIFE

### What is the meaning of life?
Depends on the context. If it isn't 42, it's yours to make.

### What is the current state of the world?
Objectively awful. Capitalism reigns as the dominant, and probably only effective, economic system and people are in shambles. Many more than need be.

Speaking from the United States of Murika here, shit's fucked.

### Is collapse imminent?
I wouldn't say within-a-year imminent, unless some tard decides to shoot us all in our feet with a terrible move, but unfortunately I and a few others see it as possible within 5-10 years. Our world simply cannot go on like this.

### Is there hope?
**Not in a hundred god damn decades.** It's still good to at least TRY, but we shouldn't expect change, rather we should educate so the chances of proper societal recovery are at least a bit higher in the future.

### Why is capitalism so bad?
Drummyfish has a whole article on his LRS Wiki about it. I don't feel like yammering about it, so go read from [there](hhtp://www.tastyfish.cz/lrs/capitalism.html).

# FOOD and DRINK

### Are food dyes okay to eat?
Only some. The list of **harmful** food dyes is as follows:
- Red 40, Blue 5, Yellow 5, Yellow 6, and anything with FD&C labels.
- Caramel Color

Some foods (thankfully) use non-toxic or at least less harmful dyes, like:
- Cochineal extract
- Annato extract
- Turmeric extract
- Using vegetable juice for color

### Is packaged/instant ramen safe to eat?
I wouldn't quite count on it. I've heard about the wax used in it being harmful, especially in large quantities.

### Are meat alternatives any better?
Not necessarily. Both plant-based meats and lab-grown meats, at least in their incarnation today, are severely fucked.

Plant-based meats are heavily based on soymeal, which itself would only be a mild concern, until you factor in what they do to make it into something that tastes and looks like meat. They add in all sorts of dyes, artificial flavorings, agents, and other chemicals to attain this image, and also use various vegetable oils to achieve the meat-like sizzle that's so iconic.

Lab-grown meats, while being substantially better in theory, is **completely retarded in today's practice**. For starters, they use "fetal bovine serum", basically the blood of cow fetuses, to incite growth in the animal cells, which typically involves having to murder the fetus to attain the blood in the first place. Also, the cells. Regular animal muscle cells--from an adult in particular--only divide so many times, as per regular function. That can't slide under the capitalist regime, so they use "immortalized cells". These cells reproduce without limit or constraint, in a manner similar to cancer cells. Ironically, cancerous tumors found in meat usually lead to the carcass being disposed of.

Yes, BOTH of these means that the typical routine of getting meat from animals, unfortunately via inhumane slaughter, is healthier than the sloppified bullshit these fucks are concocting. It is still preferable to at least find better ways of doing this (e.g. letting animals die of natural cause before butchering) but my point still stands.

### What drinks are safe to drink?
The question is, what drinks is SAFEST? Let's go in order from least safe to most safe, including water, commercial (soft) drinks, and other liquids:
- Cleaning products (bleach, soap), you will likely die of poisoning
- Radioactive waste, you will likely die of either the radiation itself or the ensuing cancer
- Liquid concrete, likely to solidify inside of you and pose as a threat
- Black water and grey water, contaminated with either human waste or soap and cleaning products
- Isopropyl alcohol, not only alcoholic but also poisonous
- Spoiled and/or expired drinks, likely contains mold, bacteria, and other shit
- Rainwater that falls off of buildings/gutters, may be contaminated with dirt and random shit
- Water from the concrete ground, likely contaminated with dirt as well
- Urine, gross and likely not fit for drinking
- Sweat, INCREDIBLY gross and also likely salty
- Cola and other soft drinks, sugary as fuck and much more likely to cause enamel decay than unfluoridized water
- Alcoholic beverages, may be safer than tap water liquid-wise but will still get you drunk as fuck
- Human blood, probably better than piss or roofwater but may have plastics, bonus points if it's your own blood
- Straight rainwater, somehow these fuckers put chemicals in the clouds and now we can't drink it safely
- Sparkling water, most likely safe but just for good measure it's under tap water
- Tap water, "enriched" with fluoride that can cause bad effects, but is still able to be drank
- Water from fruits, probably similar fate to tap water but less likely since, well, it's fruit
- Certain bottled waters w/o fluoride, may still have plastics or other things
- Spring water collected directly from the Earth, assuming proper collection methods
- Filtered water, much less contaminants, some brands still keep fluoride and accidentally miss some things
- Distilled water, least amount of contaminants and capitalist interference
- Homemade distilled water, provided it's done right it provides minimum shit and tampering

For clarification, the safety of brewed drinks depends on both the brew and the liquid. Self-made coffee grinds with distilled water is for sure safe. The same thing made with bleach is a method of suicide for 90s UNIX users.

### What meats are safest to eat?
From safest to least safe:
- No meat, we're omnivorous for a reason
- Seafood (shrimp, fish, crab)
- Poultry (chicken, turkey)
- Red meat (beef, pork)
- Processed meat (sausages, probably hamburgers, bacon)
- Artificial meats (plant-based and lab-grown)

Note that the way it is cooked matters as well. Best practice is to not use fucking oils (use butter only if necessary) and to flip frequently, about once per minute rather than when the cooking side is seared.

# CULTURE

### Free culture?
Free culture, usually associated with the free culture movement, is a term for works that are able to be copied, studied, modified, and sold, similar to how free software may be treated identically. Works use a license or wavier to accomplish this, and include all forms of art or other creations, including:
- Drawn arts (paintings, sketches, comics, digital art)
- Writings (stories, information, misc. jargon)
- Sounds, usually digital
- Music (singles, albums, MIDIs, tracker music, sheet music)
Basically, if it's something you can copyright, it's something that can be found in free culture.

### What is Creative Commons?
Creative Commons is a project made to provide several licenses for alleviating restrictions from works and allowing redistribution. The licenses, or rather specific rights applied to works, are as follows:
- CC0 (Creative Commons Zero), a public domain wavier and fallback license, recommended by LRS and me
- CC-BY (Attribution), requores attribution
- CC-SA (Share Alike), requires the derivative work to be licensed identically
- CC-NC (Non-commercial0 **Non-free license**, prohibits commercial use, violates the freedom to sell
- CC-ND (No derivatives) **Non-free license**, prohibits derivative works, violates the freedom to copy (and possibly modify)

These licenses may be used in combination with each other (commonly CC-BY-SA, etc) to provide certain rights and restrictions. We prefer to use CC0 for both media and code, as it provides absolutely ZERO restrictions.

### What are some examples of free culture works?
They come in varying licenses, which shall be mentioned here.
- LUDICRITAL: The NORMAL Webcomic (CC0), has a webcomic, story, and spoken script
- Mimi and Eunice (CC-BY-SA), has comic strips
- Nose Ears (CC-BY-SA), based on Mimi and Eunice
- Drummyfish's works (CC0), has several kinds of assets including writings, images, models, and some music
- Pepper and Carrot (CC-BY), has webcomic, source projects, and story
- You may find several on OpenGameArt, Freesound, or by contacting your local GNU/Linux user, with or without schizophrenia
- Many libre games provide free culture media, including graphics, music, sound effects, and the like. Good instances of this include Freedoom, Anarch, and OpenArena.

### TikTok-
**I will stop you right there.** TikTok is complete garbage. It fosters not only exploitative behavior, but also mental deficiencies in young men and children through its short-form content and the users encouraging things and actions only dumbshit retards would do, including walking on fragile plastic milkcrates, using "level 10 rizz" on the hot "skibidi" chick with a "gyatt" that makes curly-haired white boys across the globe "goon" , committing theft of school property, and doing dumbass dances even when they're in an active fucking warzone. I'm surprised we don't have these slices of the causasian bread pantry simmering their balls in a frying pan greased with vegetable oil in the hopes that it makes their nuts smell good.

Don't even get me started on the collateral damage that this shit causes, including a possible increase in suicidality among the sane (I feel it unfortunately), and breeding more consoomers who will eat up randon shit that corporations give them just to impress the whore in their school who dumps boys day after day like fucking water bottles. Also, forget mentioning most social media platforms, TikTok is prying their formula into the rest of the mainstream web like the cancer it is because of how viral it gets.

### Does Mark Zuckerburk zuck?
Yeah he sucks, alright. Sucks my *dick.*

# TECHNOLOGY

### What is free software?
Free software is a group of software that grants four basic rights to **copy**, **sell**, **modify**, and **redistribute** it without prior permission and for no monetary cost. Various licenses are employed to distribute programs as free software, like the GPL (copyleft, requires attribution and sharing alike), MIT (permissive, only needs attribution), BSD (permissive, can contain clausess), CC0 (public domain, doesn't waive patent rights by itself), or Unlicense (public domain, DOES waive rights to ensure complete safety).

Naturally, free software has its source code available to easily modify and study, and optionally works in tandem with other free software (libraries like SDL or APIs like those for the Linux kernel).

### What is bloat?
Bloat is a term for describing unnecessary complication, inefficiency, and/or size of software. A program is considered bloated if, for instance, it takes an unorthrodox amount of storage space, takes 5 seconds to render a rather modest model, or does hundreds of mathematical calculations and loads a text file just to say "Hello, world". Common examples of programs considered bloated include systemd, Xonotic, GNOME, KDE Plasma, and anything made by Big Tech.

### Why is Windows so bad to you LRS guys?
Winshit was made by a corporation with a shady history, being filled with bloated applications and spyware, ignoring consumer repair via lack of openness and deliberate deprecation, recently requiring bullshit specs just to install on a system, and generally being abusive to its users, especially in the department of privacy.

TL;DR it's abusive, ignorant, uselessly bloated, and dogshit.

### What about MacOS?
MacOS sucks ass as well. Apple as a whole sucks so much ass you'd mistake them for either a prostitute or the money-vacuums their products are, costing several times the usual price for a modest laptop and being shipped with malware and proprietary spyware. Same shit as Windows, worse execution. *Somehow.*

### ...Linux?
Linux sucks, although it sucks less than Windows or Mac merely due to its status as free software. It's bloated to high hell, usually requiring proprietary firmware to work, and while not quite as abusive or as close to such shit mountains as Windoze, it may get there at some point, considering Microsoft's grubby little mitts delving into the "We <3 Open Source" bullshit.

### What the hell am I supposed to use them, smartass?
Either a \*BSD or Linux-based system. Yes I complained about Linux's shortcomings, but it's still much better than having Microsoft or Apple watch you like a rat in a cage.

### What kind of a PC should I get?
If you can, get one that either is from the early 2000's or one from Vikings. If either are unavailable, which they likely may be, you can at least get a ThinkPad or some basic desktop tower. Personally I don't worry too much about proprietary firmware as you probably can't wipe your ass without it, but bonus points if you get a PC that lets you remove it or comes without it.

## VIDEO GAMES

### Do video games cause violence?
A polarizing question indeed. I believe it's not the video games themselves, but the thought process of people who play them. Violence in video games can be easily differentiated from violence in real life, but the players, depending on their mental health and standpoint, may get inspired to do something or fantasize about doing something. So yes, but on the condition that the player is mentally unwell or prone to violence.

There's a thought I heard that video games don't make violent people, but more that violent people are drawn to video games.

### What games perform well on low-spec machines?
Heavily dependant on what you consider low-spec. For a white boy's frame of reference, I'll be using a Raspberry Pi 400.

- Anarch by Miloslav Ciz (drummyfish). Practically designed for running on diverse sets of hardware with efficiency.
- Most if not all games made with SAF.
- Quite a few libre games, typically 2D ones like SuperTux, Hedgewars, C-Dogs SDL, Wake to Hell, or FreeDink.
- Doom source ports, preferably played with Freedoom.
- Quake 1 source ports, preferably with LibreQuake.
- Quake 2 (no free alternative) and Quake 3 (OpenArena) may run fairly, although Q3 requires hardware acceleration.
- Nut sure at all about this, but Minetest MAY be an option. Don't trust it.

### What games are small?
- Any game by Miloslav Ciz (drummyfish), even Steamer Duck. They top at most a few megabytes in size.
- Wake to Hell by blitzdoughnuts (that's me), about under 5 megabytes including Winshit DLLs and other executable stuff
- SuperTux versions at or before 0.3 are usually under 60 megabytes, at least when zipped
- Freedoom, assuming you get both phases and FreeDM, totals around 75 megabytes. An individual IWAD is around 20 megabytes.

### Why are games so big nowadays?
It's likely due to advancing technology. As computers get more powerful, people find new opportunities to do things previously unthinkable, which include high-res textures, high detail models and geometry, complex shaders, and tolerance of piles upon piles of dogshit bloat. Of course, this massive amount of data must be stored somewhere, preferably in the least efficient ways... Assets don't need to be so high definition, ESPECIALLY if it's from far away. Nobody can distinguish 1 million polygons from 10 when the object in question is 1000 meters away.

## RANDOM

### Is the rizzler in Skibidi, Ohio going to jail for nutting in the level 100 gyatt of a sissy femboy and not asking what color his bugatti is?
...excuse me?

### No seriously, what the fuck does that mean?
Here's a rudimentary dictionary I made of some common "brainrot" terms that I always hear in my fucky-ass school.

- *rizz*: Synonym with *charisma*
- *rizzler*: An individual with impeccable charisma.
- *alpha*: The superior.
- *sigma*:
    - 1. One beyond superior to alphas.
    - 2. Random filler word
- *skibidi*: Random filler word, meaningless otherwise.
- *gyatt*: A large posterior, usually belonging to a female.
- *Fanum tax*: To steal food from someone.
- *Ohio*:
    - 1. One of the many United States.
    - 2. A location where unusual occurrences happen.
- *fr*: Abbreviation of "for real"
- *fire*: Used to describe something as being great.
- *cap*: Synonym with *lie*
- *brainrot*:
    - 1. Sloppy short-form content made to catch attention and abuse the viewer's dopamine. Usually seen on platforms like TikTok.
    - 2. Used to refer to the dialect being defined here.
- *on God*: ???
- *illiterate*: Any elementary school student born after 2015.
- *goon*: To engage in edging.
- *gooner*:
    - 1. One who goons.
    - 2. A depraved pervert who can only be sated by bringing their ill fetishes to innocent bystanders. See e.g. Giggly Goonclown.

### What is They Live?
A documentary.

### How come you haven't went to alcoholism yet?
I must remain sober so I can identify the retardation around me with accuracy.

### What is sex? My brother won't tell me what it is.
Sex is a shorthand for sexual activity, which is an activity done by 2 or more people that involves sexual satisfaction, genitals, or a similar tension. Sex may be done for many reasons, including starting a family, as a means of affection, for monetary gain (prostitution), or just because neither of them have shit to do. There are several different ways that people perform sexual activity, including intercourse (inserting penises, objects, or phallic, penis-like objects into an orfrice), outercourse (stimulating genitalia or erogenous areas  of another without penetration, e.g. with the hands, breasts, or buttocks), mutual masturbation (all parties stimulate themselves, usually with visual assistance from other participants), and more recently a form of long-distance sex (basically masturbation aided with sex toys controllable by the other partners, geared for operation from miles away).

Sex is a very important part of human life. Everyone learns about it some time during theur lives, usually early in, people tend to do it with others, and humans (traditionally) can't reproduce without sexual intercourse. Especially more recently, sexual deviation has become much more prevalent. By this, I mean kinks and fetishes, including breast-oriented ones, feet, odors, gore, furries, plushies, and fat fucks including but not limited to Reddit users and your mom.

A list of important sex-havers goes as follows:
- Any mother or father, including yours
- Redmond and Bluu Hueston (from blitzdoughnuts' drawings)
- Furries
- The local red-haired hoe who may be in your town (we know she gets a FUCKTON of dick, because she acts like one)

### Is this a schizo-post?
Depends on your perspective. For instance, looking at it from my eyes, yes and it's a good thing.
