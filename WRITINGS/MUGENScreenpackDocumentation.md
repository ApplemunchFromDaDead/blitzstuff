MUGEN / Ikemen GO Screenpack Documentation
by blitzdoughnuts
Text is licensed under CC0

================

I've hardly ever seen documentation on the Ikemen GO motif system, so I decided to crack into the default Ikemen GO screenpack and make some documentation of my own.

# system.def
system.def is the most important file in a screenpack, constituting the menus and such.

## [Info]
- nane: A string for the name of the motif
- author: A string for the author
- versiondate: The date in MM, DD, YYYY format, e.g. 04,20,2024
- mugenversion: A decimal number denoting the MUGEN version, usually 1.0 or 1.1
- localcoord: The W,H local resolution of the screenpack. Usually 320,240

## [Files]
Files in MUGEN / Ikemen GO, if explicit directories aren't given, will be read from the following directories, using the executable directory as the root:
- /data/motif/file.txt
- /data/file.txt
- /file.txt

The Files header uses the follwoing entries:
- spr: A .sff file, contains sprites
- snd: A .snd file, contains SFX
- logo.storyboard: A logo storyboard, appears before the intro. Optional
- intro.storyboard: An intro storyboard, leads to the menu. Optional
- select: Points to a .def file for what characters and stages to load
- fight: Points to a .def file that makes the in-game HUD (life bar, power, names, etc.)
- fontX: Points to a .def file that defines a font for use in the menus. The X can be a number starting from 1, like font1
- module: An Ikemen GO exclusive, points to an .lua file to be loaded for external code.

## [Music]
This entire section contains pointers to music. In MUGEN, they can be .mod tracker music, MP3s, or MIDI files. In Ikemen GO, all of these are supported (to my knowledge) as well as .ogg files from personal testing.

All entries have the following sub-entries:
    - bgm: Points to a music file
    - bgm.volume: A decimal number that represents how loud the music is.
    - bgm.loop: A 1 or 0 boolean that says whether it should loop
    - bgm.loopstart: The starting loop point for the music. Format changes based on the music type used.
    - bgm.loopend: The ending point of a loop. Same quirk as the last one.

The following entries can be used:
    - title: Title screen music
    - select: Character selection music
    - vs: The versus screen music
    - victory: Victory / winquote music, overridden by stage-defined music
    - option: Options menu music
    - replay: Replay menu music (Ikemen GO only)
    - continue: Continue screen music in Arcade mode
    - results: Mission results screen, for Survival or Time Attack modes
    - hiscore: High score screen music, both when viewing and entering a score

## [Title Info]
Yes, it has to be Title Info specifically. The most important part of the most important file, it defines the title menu.

- fadein.time: Time taken to fade in
- fadeout.time: Time taken to fade out
- menu: The prompt allowing the player to pick a mode or a menu
    - pos: X,Y coordinate for where the menu is put
    - item.font: Font in FONT, GROUP, SPRITE format to use for menu items
    - item.active.font: Font in F,G,S format to use for the highlighted item
    - item.spacing: X,Y offset between menu items
    - window.visibleitems: The max amount of visible items at a time in the menu "window"
    - window.margins.y: Not sure
    - boxcursor.visible: Boolean for the box cursor's visibility
    - boxcursor.coords: X,Y,W,H coordinates for the box cursor, relative to the menu item selected
- cursor.move.snd: Sound for moving the cursor
- cursor.done.snd: Sound for selecting an item
- cancel.snd: Sound for going back

## [TitleBGdef]
Defines the background of the menu. No shit, sherlock. Also has some sub-headers, hoo boy.

- bgclearcolor: A R,G,B color to use as the basic background color behind any sprites

### [TitleBG X]
A sub-header used to define a background element.

- type: A string that takes *normal* or *parallax*
- spriteno: A G,S sprite to use for this element
- start: An X,Y coordinate for the position of the element
- width: A W,H coordinate for the sprite's size
- tile: A double-boolean in X,Y format. Enables tiling horizontally (X) or vertically (Y).
- trans: Basically a blending variable, takes *add*, *sub*, or *mul*.

## [Select Info]
Another one of the important things in this important thing, accessed by an important thing. Trust me, I'll be repeating myself much more in this.

- fadein.time: Same as Title Info
- fadeout.time: Same as Title Info
- rows: The integer "height" of the character boxes
- columns: The integer "width" of the character boxes
- wrapping: A boolean to allow cursurs to wrap around the character boxes
- pos: The X,Y position of the character box region, at the top left
- showemptyboxes: A boolean that allows a box with no character or an invalid character to still appear with a background.
- cell: The cells that contain the characters to choose.
    - size: The W,H size of the cell
    - spacing: Pixel distance between cells plus their size, goes both directions
    - bg.spr: The box sprite that appears behind the character's icon
    - random.spr: The random character icon
    - random.switchtime: The amount of frames to wait before picking another random character when hovering over the random character portait
- p1 and p2: The player 1 and player 2 elements that share the same common sub-elements.
    - cursor.active.spr: The cursor sprite to use when still picking a character
    - cursor.done.spr: The cursor sprite to use when a character is selected
    - cursor.move.snd: The sound to play when moving
    - cursor.done.snd: The sound to play when choosing a character
    - cursor.random.snd: The sound to play when you want to annoy the player for hovering over the random character
    - cursor.startcell: The cell for this player's cursor to start on
    - cursor.blink: Really only used for p2, says whether p2's cursor should blink if it overlaps p1's cursor
    - face.spr: The CHARACTER'S SPRITE to use as a big portrait. Usually, we use 9000,1
    - face.offset: The X,Y position of the big portrait
    - face.scale: W,H scale in decimal (1 = 100%)
    - face.facing: A boolean: 0 is facing left and 1 is facing right, assuming the sprite used is made facing right by default.
- portrait: The character portrait.
    - spr: The CHARACTER'S SPRITE to use as an icon. Usually, we use 9000,0
    - offset: The X,Y offset from the icon's cell
    - scale: The W,H scale in decimals to use. 1.0 (or 1) = 100% scale.
- title: The title text, changes based on what mode is being played
    - offset: The X,Y offset from 0,0 to use
    - font: The F,G,S font to use for this text

# fight.def
The in-game HUD you see when fighting that has the lifebar, powerbar, all that good stuff.

## [Lifebar]
An essential part; this displays your life, name, and anything else.
