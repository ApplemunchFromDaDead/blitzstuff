QUOTES from the LRS WIKI
compiled by blitzdoughnuts
Licensed under CC0, uses Less Retarded Wiki excerpts that are also CC0

== BACKGROUND INFO ==

The LRS Wiki, or the Less Retarded Wiki, is a CC0, entirely public domain encyclopedia that focuses on tech and ethics, specifically minimalism, free software, and bloatlessness in software, and anarchism, pacifism, communism, anti-capitalism, and universal, unconditional love in ethics. It reflects the thoughts and worldview of its author, Milocslav Ciz aka drummyfish.

As a follower of his ideeas and philosophy, and also a lurker of his, I (blitzdoughnuts) have spent quite a bit reading the LRS Wiki, either seeking information or a way to entertain myself. Boy, does the LRS wiki entertain. This quote compilation consists of quotes from the LRS Wiki that I personally find rather humorous.

[ Note that comments in SQUARE BRACKETS like this one are comments from me, not drummyfish. -blitzdoughnuts ]

=====================

In "proprietary":
...you should avoid proprietary software as much as possible (considering in today's society you probably can't even take a shit without using some form of proprietary software).

In "sjw":
SJWs say the term is pejorative. We say it's not pejorative enough xD

In "political_correctness": (discussing pseudo polical incorrectness)
It goes along the lines of "IN TIMES OF POLITICAL CORRECTNESS, this SUPER HERO CELEBRITY IS A COOL NONCONFORMING REBEL who is not afraid of saying the word fuck in a youtube video". Of course this is retarded as fuck, ...

(immediately after the paragraph about pseudo-PIC)
{ LMAO I just almost shat myself, I heard someone in a video say "vertically challenged", with a COMPLETELY SERIOUS FACE -- I was like WTF is that, it was a bit of a headscratcher for me till I looked it up on the net and found it just means someone who is short :'D I fucking can't anymore with this shit. Bruh I wonder what horizontally challenged would mean, is it like someone who's real fat? ~drummyfish }

In "sanism":
LMAO imagine future news be like "Mentally divergent age fluid human people person of unspecified non-hexadecimal gender and afro american ethno-social-construct was arrested after an incident involving guns and liquor stores. No harm was intended during saying this sentence and we apologize in advance for any mental harm that may have been caused to mentally sensitive people persons by hearing this sentence."

In "gender_studies": (NOTE: this is the entire page)
what the actual fuck

In "woman":
{ I actually enjoy women football, mostly for its comedic value. ~drummyfish }

In "gay":
{ I even observed this effect on myself a bit. I've always been completely straight, perhaps mildly bisexual when very horny. Without going into detail, after spending some time in a specific group of people, I found my sexual preference and what I found "hot" shifting towards the preference prevailing in that group. Take from that whatever you will. ~drummyfish }

In "disease": (listing off numerous diseases)
- obsessive shitposting (for example writing a wiki)

In "consumerism":
[ Literally the entire consumerism rant from drummyfish. It's less funny than it is depressing and accurate, but it's still got a bit of humor to it. -blitzdoughnuts ]
