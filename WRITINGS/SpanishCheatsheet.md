SPANISH CHEATSHEET
by blitzdoughnuts\
Licensed under CC0\
Written in English

================
I want to preface this by apologizing for the lack of proper punctuation. This is typed on a USA keyboard by some American who wants to learn Spanish because he wants to understand the inner machinations of a few Spanish-speaking people.

== Pronouns ==

Tu = You (subject)
Te = Yourself (subject)
Ti = You (subject, informal)

El = He (subject)
Ello = She (subject)
Nosotros = We (subject)
Vosotros = You (subject, formal)

Esto, Este, Esta = This (subject; neutral, masc, fem)
Ellos, Ellas = They (subject; masc, fem)
Se = Themselves

La, Los = That, It

Ellos = ???
Ellas = ???

== Suffixes ==

-a is commonly used for present tense

== Basic Words ==

En = In
Es = Is
A = To
Y = And
De = Of, From
Pero = But
Un = A (as in *a* glass of water)
Ser = Be, Is (permanent, verb)
Estar = Be, Is (temporary, verb)
Por = By, Because of
Que = What
Para = For
Como = Like, As
Puedo = Can

Asi = That way

== Specific Words ==
For these, use the tense of the English word.

Muerto = Dead (<subject> is dead)
Muerta = Die? Dead? [PROBABLY INCORRECT]
Muriendo = Die
Muertas

Herida = Injury, Wound (physical)
Herido = Injured (physically from an accident), Wounded (physically from an attack)

Dolido = Injured (emotionally)

Mirar = Look (may also be "Look at" or "Watch")

Comprende = Understand

Saber, Conocer = Know
Se = Know

Cosa(s) = Thing(s)

Clasico = Classic

Nombre = Name

Leche = Milk

Libre = Free (freedom wise)
Gratis = Free (price wise)

== Example Sentences ==

"El es muerto a mi." - He is dead to me.
Breakdown: El (He) es (is) muerto (dead) a (to) mi (me).
"Muerto" is in past tense

"Quiero tu cabeza!" - I want your head!
Breakdown: Quiero ([I] want) tu (your) cabeza (head)!
"Quiero" is in present tense

"Ello estar herido!" - She is injured!
Breakdown: Ello (She) estar (is) herido (injured)!
"Estar" is used as she will hopefully not be injured for long.

"Por que me mirar asi?" - Why are you looking at me that way?
Literally "Why me looking that way?"
Breakdown: Por que (Why) me mirar (looking at me) asi (that way)?
Note that, compared to English, verbs have more leniency with their placement. With literal-minded translations, you may have to guess or swap words around.
Also, verbs can lack their subject based on their properties (e.g. "tu mirar" is technically redundant)

"Mirar mi, pero tu no mirar todos mis." - You see me, but you don't see all of me.
Literally "See me, but you no see all me."
Breakdown: Mirar (You see) mi (me), pero (but) tu (you) no (don't) mirar (see) todos (all of) mis (me).
"Mis" is used instead of "Mi" because when referring to plural things you have, you say "my things" in English, and "mis cosas" in Spanish.

"Ellas peudos se joder." - They can go fuck themselves.
Breakdown: Ellas (they) puedos (can go) se jude (fuck themselves)
Another instance of odd verb placement.

"Y uno para tu perro." - And one for your dog.
Breakdown: Y (And) uno (one) para (for) tu (your) perro (dog).
