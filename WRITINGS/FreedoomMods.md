FREEDOOM MODS
list by blitzdoughnuts
Licensed under CC0

================

Here are some cool mods I found made specifically for Freedoom. I can't guarantee their liberation, but I can guarantee their Freedoom compatibility.

- Attack of the Trilobites by blitzdoughnuts; a small level pack for Phase 2 where you regain control of an infested outpost. **Vanilla-compatible.**
- Defend the Statue by blitzdoughnuts; a horde invasion mod where you must protect a statue from hordes of monsters. **Needs a ZDoom-based port.**
- Hideous Destructor by mc776; a realism mod that adds inventory management, wounding, stamina, etc. **Needs GZDoom.**
- Freedoom: Badass Edition; "the most bad\*ass freedoom mod on earth" **Needs GZDoom.**
    - BadAGMss; a liberated fork of the above mod **Needs GZDoom.**
