# Making MUGEN the LRS Way

So you want to make a character for the MUGEN (or IKEMEN GO) engine. That's good to hear, let me show you how to do it PROPERLY.

## Getting the Tools

A character is composed of code, sprites, sounds, and text files. Assuming you're a seasoned freetard, you've already got a text editor like vim or nano. Note that this means that **you'll need to manually type out hitboxes in the character's .air file.**

This leaves the sprites and sounds. MUGEN doesn't use traditional PNGs and WAVs for character assets. You'll need some special tools, but unfortunately not many FOSS tools are available for the MUGEN engine's files. Here, I'll list whatever FOSS tools I can find:
    - [Nomen](https://sourceforge.net/projects/nomeneditor/), a MUGEN sprite editor.
    - **NEED A SOUND EDITOR!!!!**

## Making the Character

Now that you have the tools, it's time to get crackin! Making this character the LRS way, making it free culture is a necessity. This means you can't just FOSS the code, you need to FOSS EVERYTHING. The sprites, the sounds, EVERYTHING. Unless you're basing off of an existing FOSS character, this will take quite a bit of artistic effort.

### Technical Details

Assuming you're not the fightiong game type, here's some basic balancing rules and terminology:
    - The amount of frames an attack takes to play out its animation and at which times the hitbox is out is deemed "frame data". Lighter attacks tend to be quicker but deal less damage, and vice versa for heavier attacks. Due to attacking speed, there are usually gaps in them that allow for the opponent to potentially intercept a hit by by dodging it and then striking, or catching it with their own strike. Keep this in mind when making attacks.
    - Many special moves involve strings of inputs, more often than not having the control stick.  Common motions are:
        - Quarter circles; *D, DF, F* in MUGEN controls for a forward QC. Notice how it starts from down, and careens into forward.
        - Half-circles circles; *B, DF, D, DF, F* in MUGEN controls for a forward HC. It's like a quarter circle, but you start from back.
        - 360s; *F, Fu, U, BU, B, DB, D, DF, F* in MUGEN controls for a forward 360. This particular 360 starts and ends at forward.
    - Moves tend to have certain flaws that give balance. For instance, a strong punch will be hard to hit crouching characters with, or a special projectile move has a slow wind-down.
    - Attack hitboxes in IKEMEN / MUGEN can be given unique properties, like whether they can only be blocked when standing or crouching, their "chain ID" for combos, the amount of knockback they deal when hitting the enemy in various states (hit standing, hit in the air, blocked crouching), and the amount of damage dealt even when blocked.
    - When a character is idle, the hitboxes should stay the same through the entire idle animation, unless there is a good reason. For example, if the character only breathes or otherwise moves subtly, a change in hitboxes isn't necessary.

### Design Guidelines

Some basic standards that are basically universal across many MUGEN characters are as follows:
    - There exists special moves (performable by a short combination), EX moves (powered versions of special moves that use power, usually improves damage or speed), and hyper moves (moves that have great power on their own and tend to use whole "stocks" of power).
    - Blocking is automatically done when holding the direction opposite to the character's direction. Two characters always face each other, assuming they're opponents of each other. Walking backwards is an option, but blocking activates when an opponent is attacking or a projectile is incoming, usually within a range of the character.
    - Usually, characters have up to 3 stocks of power, usually 1000 power each. This power builds as they perform (yes, perform), land, and/or sustain attacks.
    - A character shouldn't be able to chain a single move repeatedly for more than 4-5 hits, assuming the attack isn't blocked and the attack in question only hits once.

## Alternatives

Just use the Anarchic Brawl engine. It's libre, it supports PNGs and WAVs, it's much more minimalist and suckless than Ikemen GO, and generally an ethical improvement. It's incompatible with Ikemen GO / MUGEN characters, however.
