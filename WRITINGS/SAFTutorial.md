# SAF Tutorial by blitzdoughnuts

**This tutorial assumes you are in a BASH environment ((GNU/)Linux bash, w64devkit), downloaded the entire SAF repository, and have a C compiler and Python3.**

Don't be shy to copy from this tutorial, just don't be lazy and leave in the clutter (comments, extra tabs/spaces, etc); it'll make your code look a little more like shit.

This tutorial is licensed under CC0. Share the love!

## General Guidelines

Remember: when you make a game for SAF, imagine that you're making a game for something like a Pokitto or some other low-end platform.

Good practice is to only use integers and, if decimals are needed, emulate it by "splitting" integers into fractions or using division/bitshifts. Don't use what you don't need, like if you don't need music or more complex rendering than simple blocks, shapes and sprites, why bother?

Since SAF utilizes C and doesn't try to give you everything you need like over-complex objects with tons of unnecessary variables, you'll want to program your game logic **manually.** Try learning to program in C first. If you can program in C++, that'll work too, but please note that C++ is, in LRS terms, bloated dogshit, and many things C++ can do, C can also do in a different fashion.

## 1: Making a Base Game

First, make sure you have **saf.h** inside of the directory you intend to put your main C file in. Make a file for your main C program, like *main.c*. Inside, give it the following content:

```
#define SAF_PROGRAM_NAME "SAF Game"    // replace with game name, e.g. Anarch
#define SAF_PLATFORM_SDL2              // SDL2 for PC

#include "saf.h"

void SAF_init() {};                    // init function: can be blank

uint8_t SAF_loop() {                   // per-frame function, SAF handles the actual program loop (25 FPS)
	SAF_drawText(
		"Hello World!",                // text
		0,                             // X position
		0,                             // Y position
		7,                             // text color (can be a number or a SAF_COLOR_*)
		1                              // text size (CANNOT be a decimal)
	);
};
```

Compile your program with, e.g. ``gcc main.c -lSDL2 -o game`` and run it to see if it works.

## 2: Adding Sprites

To make a sprite, you should use the Python file **img2array.py** in the *tools* folder of the SAF repository. You need a PNG file, preferably with a size that is a common factor of 2, like 16x16.

Execute ``python3 img2array.py img.png > img.txt`` to invoke the Python script and send the output to a file. It'll look something like this:

```
uint8_t image[...] {
	...
};
```

Feel free to change the name of the image to whatever you want. You can do this by using ``-nNAME`` before ``img.png``. Enter the command without any parameters to get a help message.

To draw the sprite, *#include* the text file to your program and insert the **SAF_drawImage(sprite, x, y, tansform, transparentColor)** function inside of your SAF\_loop() function, like so:

```
SAF_drawImage(image, 0, 0, 0, 0);
// we can use 0 for transform and transparentColor since 0 is "no transformation" and "pitch black" respectively.
// the Python script converts transparency to pitch black by default
```

## 3: Playing SFX

SAF has a dedicated function to play sounds: **SAF_playSound(index)**. By default, only 4 sounds can be played:

- **SAF_SOUND_BEEP** for a harsh beep.
- **SAF_SOUND_CLICK** for a clicking-esque sound.
- **SAF_SOUND_BOOM** for something like a gun firing or something exploding.
- **SAF_SOUND_BUMP** for hitting/slamming into something like a wall.

Your destination platform may not have sounds, whether by device limitation or a defined setting (SAF\_SETTING\_ENABLE\_SOUND). In that case, it may print a message or change the window title as a substitute.

For an example:

```
if (player.x >= wall.x && (wall.flags & FLAGS_WALL_BLOCK_RIGHT)) {
	SAF_playSound(SAF_SOUND_BUMP);
	player.x = wall.x;
	player.hsp = 0;
};
```

## Porting a Game

If you have a premade game, preferably made in C and with a separate file containing game logic, it should be decently trivial to port it to SAF, at least in a rudimentary way. One example is Wake to Hell, a predominantly SDL2 game with separated game code and a very rudimentary SAF port.

Depending on the type of game it is, it either can be perfectly ported, ported with drawbacks, or is outright impossible to port it without modifying SAF itself to alleviate any harsh limitations. Speaking of drawbacks, these may include the following:
- The limited, somewhat basic color palette of RGB332
- The presence of only 6 buttons (can probably be up to 10 by using A or B as a "modifier" button)
- The 64x64 resolution, very tight and not so welcome to details
