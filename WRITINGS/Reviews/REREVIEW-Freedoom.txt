Freedoom is a free and open source project made to provide replacement assets for id Software's classic DOOM games that are completely free of price and free to use, which if combined with the many GPL licensed source ports, makes a completely libre game!

Of course it has to be different: you're not fighting demons, you're not in the middle of a demonic invasion, there are no nazis. You're fighting... bugs. And worms. And also some other extrateresstrial critters, and also Minigum McGee.

One goal in mind is to both keep Freedoom compatible with vanilla ports, and make it compatible with as many DOOM mods as possible. Worst case scenario, something'll look or sound off, or will just not make sense at all. Best case scenario, the mod is made with Freedoom in mind. But most of them don't bother.

Doesn't matter what the mod is. If it exists, you can run it with Freedoom. Yes, this includes cult classics like Brutal Doom, Hideous Destructor, Reelism, ModOhFun, Mock 2, HDoom, Moon Man in DOOM, Grezzo 2, and also a fair assortment of Terry WADs. That's how you KNOW Freedoom isn't fucking around.

Also, there's FreeDM, an official deathmatch level pack that runs as its own IWAD. Unfortunately, not many people give a shit about it, especially considering that the soundtrack is just as killer as the rest of the music in the project.

In short, you can think of Freedoom as a pest extermination total conversion with a pinch of an organization of dimwits that, of course, has to make everything miserable. And it happens to be completely free. Have fun!
