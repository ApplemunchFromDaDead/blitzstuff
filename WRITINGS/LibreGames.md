# LIBRE GAMES
by blitzdoughnuts\
Licensed under CC0

================

This is a list of games that are free software and free culture, with honorable mentions to those who don't quite make the cut.
A game will be listed as so:
- [Nutrix: The Quest for The Less Retarded Stone](http://www.tastyfish.cz)
	- License: CC0 (code and assets) [LICENSES]
	- Programmed in C using the SAF engine, adheres to suckless/LRS philosophy, the dev doesn't have a sense of smell [TECHNICAL DETAILS, NOTES]
	- Around 128kb big [FILE SIZE]

## COMPLETELY FREE GAMES

- [Anarch](https://codeberg.org/drummyfish/Anarch)
	- License: CC0 + wavier (code and assets)
	- Programmed in C, specifically C99, suckless and LRS, uses custom rendering backend (raycastlib)
	- Whole game is <256kb big, can get smaller with procedurally generated assets
- [Freedoom](https://freedoom.github.io) [3]
	- License: BSD 3-Clause (assets), GPLv2 (code)
	- Game is a set of assets to use with the Doom engine for a free game
	- One IWAD is about 20-25mb big, totals to about ~75mb for all zip files
- OpenArena [3]
	- License( GPLv2 (code and assets)
	- Programmed in C with the ioquake3 engine, modifies ioquake3 engine
	- Whole game is about 200mb big, minus auto-downloaded content
- Xonotic
	- License: GPLv2 (code and assets)
	- Programmed in C and QuakeC using the Darkplaces engine
	- About 1gb big!!
- LibreQuake [3]
	- License: BSD 3-Clause (assets), GPLv2 (code)
	- Game is a set of assets to use with the quake engine for a free game
	- About 200mb big or so, comparable to OpenArena
- MineTest
	- License: GPLv3 (code), various free licenses (assets)
	- Programmed in C++ with the Irrlicht engine
	- About ~30mb big
- Wake to Hell
	- License: CC0 + wavier (code and assets) [2]
	- Programmed in C with SDL2, currently aims to be suckless/LRS, drawing is handled ENTIRELY by frontends currently
	- About ~500kb big for SDL2, excluding executable and any Windoze DLL files

## ARGUABLY / SOMEWHAT FREE GAMES

- SuperTux
	- License: GPLv2 (code and assets), has ambiguous licensing
	- Programmed in C++ with SDL2 and OpenGL
	- About ~250mb big (0.6.3), older versions are <50mb big
- Cube 1
	- License: GPLv3 (code), varying including freeware (assets)
	- Programmed in C++ with OpenGL, quite efficient, uses SDL 1.2
	- About ~3-5mb big
- Cube 2: Sauerbraten
	- License: GPLv3 (code), varying including freeware (assets)
	- Programmed in C++ with OpenGL, successor to Cube 1 engine, still quite efficient but somewhat less due to added flair (thankfully toggleable)
	- Sauerbraten is around 1gb big!!
- Tomatenquark
	- License: GPLv3 (code), varying copyleft/permissive/Creative Commons licenses, including non-free CC ones (assets)
	- Same programming specs as Cube 2, aims for asset liberation, is on Steam :/
	- About ~250mb big
- SuperTuxKart
	- License: GPLv3 (code), GPLv3 (assets), includes possibly non-free licensing [1]
	- Programmed in C++ with fork of Irrlicht engine
	- About ~250mb big

== FOOTNOTES ==

[1] - To be specific, there are a few assets that aren't clearly licensed or are suspiciously licensed. Also, STK uses 2 characters that required permission to use--permission only granted to STK--which may be considered non-free. Drummyfish has managed to get Beastie in hot water by pointing out how his creator wished to keep rights to the character. The Debian repository's version of STK seems to have removed the two infringing characters from the game data, too.
[2] - Wake to Hell is fully CC0 in itself, yet two sound effects (dooropen.wav and doorclose.wav) are taken from Freesound, and while they're CC0, their source isn't listed, and is currently being tracked down by the game's developer.
[3] - These games have technical trade dress concerns that are excused by blitzdoughnuts, the list author, because the HUD is, at least in some way, hardcoded into the engine it uses, and it would be implausible to modify the engine to change the HUD.
