Here's a list of ideas of mine.

## Bus stop game (maybe "Next Stop"?)

This would be a game inspired by The Normal Elevator and Regretevator; you're on a bus to no discriminate location, and each stop taken is random.
Along the way, several NPCs come and go, bringing their interactions with them. These would take the idea of "the NPCs talk to each other" to a
new level with multi-NPC conversations and similar condition-dependant speech.

The game could use Anarch's engine (not raycastlib, it's ENGINE as an FPS game) or a custom small3dlib FPS engine. (Fazer much?)

Stop ideas:
- An Invasion stop where players (and maybe NPCs) use weapons to take down incoming enemies for a wave or two, maybe themed by Anarch?
- Maybe a floor that migrates players into an elevator... that just leads them back to the bus
- A forest that people will simply get lost in

## 3D FPS Game (Fazer)

This would be a game inspired by Anarch and would be a Quake to its Doom. It would use small3dlib and have 3D models for all things interactible.
Ideas for how it could be implemented:
- The level is a 3D model exported as an OBJ, converted to both S3L_Model3D and a Model3D-ish collision mesh
- Objects in a level could be represented as special vertices or points in Blender that get converted to actual map objects by a script
- The player, enemies, and anything affected by basic physics collides with the level and other physical objects, including colliding with the level's collision mesh
- Gameplay-wise, similar to Anarch in that there's no armor, a few weapons (axe, pistol, shotgun, rocket launcher, big ass firearm like The Solution), you have all weapons and picking up ammo lets you use them, the enemies are dumbasses, etc.
- Unique to the game is a form of strafejumping/bunnyhopping, crushers don't instakill, enemies are slightly more fair (they don't attack instantly and have timers), health and ammo are less scarce compared to Anarch, etc.
Some non-vital features and enhancements for after the basic game is functional:
- Have a performance mode that turns enemies, viewmodels and pickups into sprites and disables floor texturing
- Let frontends opt to provide their own music and sprite drawing techniques (maybe with a macro like SF3_FRONTEND_MUSIC or SF3_FRONTEND_DRAWSPRITES)
- Hell, let frontends ditch S3L rendering and use their own? (will we finally get a small3dlib OpenGL wrapper?)
- Model interpolation (model anims will have no interp, we can add linear interpolation so players will gouge their eyes out less)

4April 4th, 2024 update: There's a few issues I've come to when developing the core of a game under this:
- I have ZERO idea how to do collision detection between an AABB rectangle and a collision mesh, let alone a triangle or polygon
- How do I make enemy AI? I imagine it is very simple for Anarch; just move along tiles if they're step-high or lower, and follow the player. However, this game doesn't work on tiles, and it'll likely take a good effort to do step-high solid detection in comparison.
- That's it.

Thankfully I have ideas for alleviating a few issues I'd stumble across:
- For making the collision mesh, maybe just use the level's model? (maybe vice versa with my current layout)
	- Maybe use two circles (1 bottom, 1 top) for the player and enemies? Circle collision can be less expensive, especially if other solid interactibles use them
- I would try casting lines for hitscan, but why not make bullets projectiles like Anarch? Really fast projectiles.
- I already fixed the issue I had with viewmodel positioning by just hard-baking the position into the model. I also plan to make it so that you can use a separate camera for rendering the viewmodel so it won't go through walls, and can have a separate FOV
- With the enemies, the step-high collision could simply be done the same way that the player's could; moving into a solid? Check how high it is compared to your origin. If it's, say, a 128 unit diff, step up.
- Alternatively, make the enemies dumb as fuck. Anarch did that too (and they even go into each other), so no shame there.

## Colliding with a 3D mesh

Considering my issue I'm having with 3D collision, here I will brainstorm some ways to **collide a bounding rectangle with a line between two points and a 3-point triangle in 3D space.**

For colliding a rectangle with a line, we must see if any part of the line intersects or touches the rectangle. A brute-force way would be to do a gradual interpolation from A to B, and each step check if the C, the point interpolating between the two, is inside of the rectangle at that moment. Very likely to work, but very expensive and most likely laggy. Some optimizations could be made, like doing "subdivisions" and checking those iteratively rather than swirling down the whole line.

## Fully libre Terry WAD

You know those Doom mods that have that SOMEMONG texture from Skulltag and rape your senses like the enemies within the demented levels rape your character? What if there was a mod just like those, except it's fully libre? Some spitballs:
	- Richard Stallman replaces Carnevil / "Terry"
	- Romeromen are replaced with the Freedoom brains, probably just name em Neurofags?
	- From experience, many Terry WADs use music from third party sources, mainly talking about OKB.wad here. Maybe incorporate MIDI versions of some libre game music?
	- Use ZDoom's game filter to fuck with people who use the standard Doom IWADs :)
		- Include Freedoom: Phase 2 with the Terry WAD so the Terry enthusiasts can enjoy the raperain correctly
